# Test description

This is a description of the tests that were added to test the snapshot feature and related components.

## applyIndexToLog (this always happens now)
[[apply-index-to-log.test.js](../test/apply-index-to-log.test.js)]

`applyIndexToLog` was initially implemented as an optional feature, but now it is part of the standard index snapshot functionality and cannot be disabled. The idea of `applyIndexToLog` is to have the entries from the index snapshot also inserted in the operation log instead of just the index. This way, we avoid situations where an old entry in the log would overwrite a more recent index entry.

### 2 nodes and old snapshot
There are 2 nodes. Two documents are inserted in node 1: {A=0} and {B=0}, and then a snapshot is taken. Afterwards, several more documents are inserted in node 1: {A=1}, {A=2}, {B=1}, {A=3}, {A=4}, {A=5} (in this order).

As the snapshot is old, we need to replicate deep enough (i.e., until doc {B=1}) to get the full state of the database.

1. should reach incorrect state - too small maxReplicationDepth
  - Here we use a small maximum replication depth (3) and so we do not replicate document {B=1}, making the state inconsistent (object B will have value 0).
1. should reach correct state - appropriate maxReplicationDepth
  - Now, using a large enough maximum replication depth (4), document {B=1} is replicated, and so both objects (A and B) have the correct state.

### 3 nodes - local and remote heads situation
Here we address the local and remote heads situation, also seen [here](./consistency.md) (but a bit differently).

1. third node should converge to correct state
The expected behavior is illustrated here:
```txt
 -------NODE 1-------    -------NODE 2-------    -------NODE 3-------
       startup
                               startup
                  <--sync-->

    write A:=0 (t0)         write B:=0 (t0)

                  <--sync-->

  -heads-                 -heads-
  local: {A=0}            local: {B=0}
  remote: {A=0;B=0}       remote: {A=0;B=0}

                            write C:=1 (t1)
                            write B:=2 (t2)
                            write C:=2 (t3)
                            write C:=3 (t4)
                            write C:=4 (t5)

                  <--sync-->

  -heads-                 -heads-
  local: {A=0}            local: {C=4}
  remote: {C=4}           remote: {A=0;B=0}

                            takes snapshot

                                                   joins network

                         sends snapshot
                             --- {A:0, B:2, C:4} --->
                         and heads
                             --- {C=4; A=0; B=0} --->
    sends heads
               ------------ {A=0; C=4} ------------->
                                                merges snapshot in index and
                                                replicates log w/ depth 2 (from
                                                heads)

                                                !!! Heads/snapshot can arrive in
                                                any order, and depending on this,
                                                result will vary

                                                If N1's heads arrive first, the
                                                log will look like this
                                                [A=0] [B=0] [B=2]  C=3 <--[C=4]

                                                If N2's snapshot arrives first,
                                                the log will look like this
                                                [A=0] [B=0] [B=2] [C=4]

                                                the heads are in [square brackets]

```

Depending on which of the heads/snapshot arrives (or is processed) first, the behavior is slightly different:

- If the heads from Node 1 arrive first, the replication process is triggered and executed with the defined maximum depth (2). That is why the log has an additional entry in this scenario.
- If the snapshot and heads from Node 2 arrive first, the snapshot is introduced in the operation log before the replication process is triggered. Consequently, replication adds heads `A=0` and `B=0` to the log but ends immediately after, as heads `B=2` and `C=4` are already in the log (from the snapshot), and `A=0` and `B=0` have no `next` entries.

___Note:___ Before introducing the `applyIndexToLog` feature, this example illustrated a situation where the database would converge to an inconsistent state - the head from Node 1 with `B=0` would overwrite the more recent state received in the index (`B=2`), as there was no other entry in the log referring to object `B`. See [this](./consistency.md) for a better explanation.


## Index Snapshot
[[index-snapshot.test.js](../test/index-snapshot.test.js)]

The index snapshot represents the full state of the database at the time the snapshot is taken. It is a copy of the store's index. It then enables new nodes to use these snapshots to initialize their state. The tests try to assess the implementation correctness of this feature.

When a snapshot is taken, the node serializes it as a string and saves the resulting file to IPFS so that other nodes can easily access it.

### Pass index snapshot IPFS hash at node's creation
After a node is created and a snapshot is taken, the IPFS hash of the snapshot is passed as an argument on the creation of a new node. On the startup process this snapshot is retrieved from IPFS and used to initialize the index and operation log on the new node.

1. instantiates a remote docstore from the local Index snapshot
1. retrieves an object from the remote docstore
1. adds an object to the remote docstore

### Get index snapshot IPFS hash during node joining protocol
Instead of passing the IPFS on the creation of the new node, the node is created without that information and then expects to receive the hash of the snapshot later (during the joining protocol). The protocol is simple: the new node joins the network and existing nodes are notified. As before, the existing nodes then send their local and remote heads to the new node, and now they also send the IPFS hash of the latest snapshot (if one exists). The joining node then uses the hash (if received) to fetch the snapshot from IPFS and does as described above.

1. instantiates a remote docstore from the local Index snapshot
1. retrieves an object from the remote docstore
1. adds an object to the remote docstore

### Check snapshot is stored correctly in IPFS
Here we take a snapshot on a node (after inserting some documents), and then verify if the snapshot is being correctly stored on IPFS.

1. retrieved local IPFS snapshot is correct
  - Retrieve the snapshot using the "local" node's instance and verify if the information contained is correct
1. retrieved remote IPFS snapshot is correct
  - Retrieve the snapshot using the "remote" node's instance and verify if the information contained is correct
1. retrieved local and remote IPFS snapshots are equal
  - Verify if the snapshot retrieved on the "local" node is the same as the one retrieved on the "remote" node


## Replication depth limit
[[replication-depth-limit.test.js](../test/replication-depth-limit.test.js)]

The replication depth limit makes the replication process halt after a certain depth of the log (which is a tree graph) has been replicated.

### List log structure
Here the log has the structure of a list, i.e., each entry has one and only one `next` entry, except for the oldest entry which has zero `next` entries. It looks like this: `hello1 <-- hello2 <-- hello3 <-- [hello4]` (the head is in square brackets).

1. should partially (according to limit) replicate the log and state
  - Confirm the state is updated according to the specified replication depth limit. With a maxReplicationDepth of 3, the replicated log is: `hello2 <-- hello3 <-- [hello4]`.
1. should successfully update log after partial replication
  - Confirm the log behaves correctly after partial replication

### Tree log structure
Here the log looks like a regular tree graph, with each node possibly having more than one children nodes (i.e., several `next` entries). It looks like this:
```txt

  [later entries]
    -----------
   |  2D   1D  |    depth 1
   |   |   |   |
   |   v   v   |
   |  2C   1C  |    depth 2
   |   |\ /|   |
   |   | X |   |
   |   |/ \|   |
   |   v   v   |
   |  2B   1B  |    depth 3
   |   |   |   |
   |   v   v   |
   |  2A   1A  |    depth 4
    -----------
  [older entries]

```

1. should partially (according to limit) replicate the log and state
  - Confirm it correctly replicates the log only until depth 3 (i.e., replicates depths 1, 2, and 3).
1. should successfully update log after partial replication
  - Confirm the log behaves correctly after partial replication

### Forest log structure (fragmented log)
This is the expected behavior:
```txt

 -------NODE 1-------    -------NODE 2-------
                        (maxReplicationDepth=3)
       startup

       write 1
       write 2
       write 3
       write 4

   -log------------
     1<-2<-3<-[4]
                               startup
                  <--sync-->
                             -log---------
                               2<-3<-[4]

                             goes offline

       write 5
       write 6
       write 7
       write 8

 -log----------------------
  1<-2<-3<-4<-5<-6<-7<-[8]

                             goes online

                  <--sync-->
                         -log------------------
                          2<-3<-[4]  6<-7<-[8]

```

1. should partially (according to limit) replicate the log and state
  - Resulting in the log: `2<-3<-[4]  6<-7<-[8]`
1. should successfully update log after partial replication
  - Confirm the log behaves correctly after partial replication. This results in the log:
    ```txt

        -------------------------
       |   ---->  9 -> 8 -> 7    |
       |  |                      |
       | [22]                    |
       |  |                      |
       |   ----->  4 -> 3 -> 2   |
        -------------------------

    ```


## Snapshot-taking Heuristics
[[snapshot-heuristics.test.js](../test/snapshot-heuristics.test.js)]

The tests here seem self-explanatory enough.

### Every 5 log entries
1. should have no snapshot after 4 inserts
1. should have a snapshot after 5 inserts
1. should trigger `snapshot.taken` event

### Every 1000 milliseconds
1. should take snapshot before 1000ms timeout
1. should have taken 2 snapshots after 2000ms

