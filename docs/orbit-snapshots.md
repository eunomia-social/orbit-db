# Snapshots in OrbitDB

How can we change OrbitDB to allow for the creation of snapshots that represent the current state of the database? The purpose of such a snapshot would be to improve the efficiency of the joining process for OrbitDB nodes. Currently, a new node needs to execute all the transactions since the creation of the DB - by executing all the operations recorded in the Operation Log (`oplog`). If the `oplog` is long (and it will eventually become long), getting a new node up to date with the DB's current state can be extremely slow.

## Operation Log _vs._ Index

Created a database of type `docstore` and added the following 3 docs:
```js
> db.put({ _id: 'hello world', doc: 'all the things' })
> db.put({ _id: 'hello world 2', doc: 'all the things' })
> db.put({ _id: 'sup world', doc: 'other things' })
```

The operation log (`oplog` - implemented with `ipfs-log`) looks like this:

```js
> db._oplog.values.map((e) => e.payload)
[
  {
    op: 'PUT',
    key: 'hello world',
    value: { _id: 'hello world', doc: 'all the things' }
  },
  {
    op: 'PUT',
    key: 'hello world 2',
    value: { _id: 'hello world 2', doc: 'all the things' }
  },
  {
    op: 'PUT',
    key: 'sup world',
    value: { _id: 'sup world', doc: 'other things' }
  }
]
```

And the `index` (`DocumentIndex`) looks like this:
```js
> Object.values(db._index._index).map((e) => e.payload)
[
  {
    op: 'PUT',
    key: 'hello world',
    value: { _id: 'hello world', doc: 'all the things' }
  },
  {
    op: 'PUT',
    key: 'hello world 2',
    value: { _id: 'hello world 2', doc: 'all the things' }
  },
  {
    op: 'PUT',
    key: 'sup world',
    value: { _id: 'sup world', doc: 'other things' }
  }
]
```
_Note:_ Despite what is shown above, what the `oplog` and `index` actually store are `Entry` objects, which contain more information (including hashes, Lamport clocks, keys, and signatures). We show only the `payload` property to ease readability.

Now, we __change__ one of the of the documents by calling `put` using an existing key (`_id`):
```js
> db.put({ _id: 'hello world', doc: 'none of the things are things' })
```

The `oplog` now looks like this:
```js
> db._oplog.values.map((e) => e.payload)
[
  {
    op: 'PUT',
    key: 'hello world',
    value: { _id: 'hello world', doc: 'all the things' }
  },
  {
    op: 'PUT',
    key: 'hello world 2',
    value: { _id: 'hello world 2', doc: 'all the things' }
  },
  {
    op: 'PUT',
    key: 'sup world',
    value: { _id: 'sup world', doc: 'other things' }
  },
  {
    op: 'PUT',
    key: 'hello world',
    value: { _id: 'hello world', doc: 'none of the things are things' }
  }
]
```
with a new entry representing the write operation.


However, the `index` looks like this:
```js
> Object.values(db._index._index).map((e) => e.payload)
[
  {
    op: 'PUT',
    key: 'hello world',
    value: { _id: 'hello world', doc: 'none of the things are things' }
  },
  {
    op: 'PUT',
    key: 'hello world 2',
    value: { _id: 'hello world 2', doc: 'all the things' }
  },
  {
    op: 'PUT',
    key: 'sup world',
    value: { _id: 'sup world', doc: 'other things' }
  }
]
```

We can see the `index` keeps the current state of the database and not the whole history of transactions. Thus, __the `index` is probably what we want to snapshot__.


## `write` events
`write` events are triggered when `_addOperation` is called (on orbit-db-store).
These events are listened by:
- [orbit-db] OrbitDB.js
  - `_onWrite` is invoked - callback for local writes to the database, updates are published to pubsub.
    ```js
    _onWrite (address, entry, heads) {
      if (!heads) throw new Error("'heads' not defined")
      if (this._pubsub) this._pubsub.publish(address, heads);
    }
    ```
- [orbit-db-store] Store.js
  - `_procEntry` is invoked.
  - Emits events related to the log and the new operation.


## Pubsub
Subscribed topic is the database address. When new message is posted, `_onMessage` is called. When the peer connects to the database, `_onPeerConnected` is called. (These are methods in OrbitDB.js) -- __CLARIFY THIS__

```js
// [OrbitDB.js]

// Callback for receiving a message from the network
async _onMessage (address, heads) {
  const store = this.stores[address]
  try {
    logger.debug(`Received ${heads.length} heads for '${address}':\n`, JSON.stringify(heads.map(e => e.hash), null, 2))
    if (store && heads && heads.length > 0) {
      await store.sync(heads)
    }
  } catch (e) {
    logger.error(e)
  }
}

// Callback for when a peer connected to a database
async _onPeerConnected (address, peer) {
  logger.debug(`New peer '${peer}' connected to '${address}'`)

  const getStore = address => this.stores[address]
  const getDirectConnection = peer => this._directConnections[peer]
  const onChannelCreated = channel => { this._directConnections[channel._receiverID] = channel }

  const onMessage = (address, heads) => this._onMessage(address, heads)

  await exchangeHeads(
    this._ipfs,
    address,
    peer,
    getStore,
    getDirectConnection,
    onMessage,
    onChannelCreated
  )

  if (getStore(address)) { getStore(address).events.emit('peer', peer) }
}
```

### Getting new changes from remote DB (`_onMessage`)

#### `store.sync(heads)` [Store.js]

```js
// [Store.js]

async sync (heads) {
  this._stats.syncRequestsReceieved += 1
  logger.debug(`Sync request #${this._stats.syncRequestsReceieved} ${heads.length}`)
  if (heads.length === 0) {
    return
  }

  // To simulate network latency, uncomment this line
  // and comment out the rest of the function
  // That way the object (received as head message from pubsub)
  // doesn't get written to IPFS and so when the Replicator is fetching
  // the log, it'll fetch it from the network instead from the disk.
  // return this._replicator.load(heads)

  const saveToIpfs = async (head) => {
    if (!head) {
      console.warn("Warning: Given input entry was 'null'.")
      return Promise.resolve(null)
    }

    const identityProvider = this.identity.provider
    if (!identityProvider) throw new Error('Identity-provider is required, cannot verify entry')

    const canAppend = await this.access.canAppend(head, identityProvider)
    if (!canAppend) {
      console.warn('Warning: Given input entry is not allowed in this log and was discarded (no write access).')
      return Promise.resolve(null)
    }

    const logEntry = Entry.toEntry(head)
    const hash = await io.write(this._ipfs, Entry.getWriteFormat(logEntry), logEntry, { links: Entry.IPLD_LINKS, onlyHash: true })

    if (hash !== head.hash) {
      console.warn('"WARNING! Head hash didn\'t match the contents')
    }

    return head
  }

  const saved = await mapSeries(heads, saveToIpfs)
  await this._replicator.load(saved.filter(e => e !== null))

  if (this._replicator._buffer.length || Object.values(this._replicator._queue).length) {
    return new Promise(resolve => {
      const progressHandler = (address, hash, entry, progress, have) => {
        if (progress === have) {
          this.events.off('replicate.progress', progressHandler)
          this.events.once('replicated', resolve)
        }
      }
      this.events.on('replicate.progress', progressHandler)
    })
  }
}
```
The `sync` method essentially stores the heads to IPFS and then calls `_replicator.load`.

`_replicator.load` fetches the entries from IPFS and eventually triggers event `load.end` (inside method `_processQueue`). `load.end` in turn triggers the invocation of `onLoadCompleted`:
```js
const onLoadCompleted = async (logs, have) => {
  try {
    for (const log of logs) {
      await this._oplog.join(log)
    }
    this._replicationStatus.queued -= logs.length
    this._replicationStatus.buffered = this._replicator._buffer.length
    await this._updateIndex()

    // only store heads that has been verified and merges
    const heads = this._oplog.heads
    await this._cache.set(this.remoteHeadsPath, heads)
    logger.debug(`Saved heads ${heads.length} [${heads.map(e => e.hash).join(', ')}]`)

    // logger.debug(`<replicated>`)
    this.events.emit('replicated', this.address.toString(), logs.length)
  } catch (e) {
    console.error(e)
  }
}
```
This method joins the logs generated with the new entries, with the instance's log. Afterwards it calls `_updateIndex`, which updates the index (current state of the database). [__IDEA:__ if `_updateIndex` takes as argument the logs, that could probably make it more efficient]


### Peer joins DB network (`_onPeerConnected`)

This is triggered first on the new peer and then on the already connected peers.

#### `exchangeHeads` [exchange-heads.js]
In the `orbit-db` module.

The connecting peer (the peer that just joined the network) is the one who invokes this method.

This method establishes a direct connection with a peer with the intention of exchanging heads (send own heads and receive heads from the peer).

The execution quickly merges with the steps above (in _Getting new changes from remote DB (`_onMessage`)_) as the `sync` method from Store.js is invoked.

#### Remote and local heads
There are two different sections of the cache reserved for storing heads. One for _remote heads_ and the other for _local heads_.

___Local heads:___ these are the latest changes the node made locally to the database -- which were at some point the heads of the log (_heads_ is plural, but I think it makes sense that this is always a single value).

___Remote heads:___ these are the latest log heads after syncing with the network. This means, the heads resulting from the last update to the log with changes coming from the network. When the log is changed locally, this does not change. But when the log changes from a network interaction, the remote heads are updated (even if the result are actually changes made by the local node - which should result in both local and remote heads being the same).

When a new peer joins, __both remote and local heads are sent to the node__ -- I initially though only the actual log heads were sent to the new node.




## Snapshot solution idea
A `createDBFromSnapshot` method would take as input an `index` object and set the DB's index to the input index. It would then only need to execute the operations in the `oplog` that happened _after_ the snapshot.

### Existing `saveSnapshot` method
The `saveSnapshot` method in the `orbit-db-store` module saves a copy of the `oplog` to IPFS (see [snapshot.js](./snapshot.js) for the snapshot of our example).

It seems to be possible to change this method to create a copy of the `index` instead of the `oplog`.

### Our snapshot methods

Our `createDBFromSnapshot` (or maybe `createDBFromIndex`) method would be equivalent to the `loadFromSnapshot` method, but would instead create a new OrbitDB instance from the snapshot.

The `saveIndex` method would take the current `index` and save it to IPFS.

## Software development plan
- Create a docstore instance from an index
- Introduce a window to limit how much of the log is read to update the index
- Write tests
- Introduce a window to limit how much of the log is retrieved from IPFS
- Write tests
- Save the index snapshot to IPFS
- Retrieve the index snapshot from IPFS
- Write tests
- Adapt the node joining protocol
- Write tests


### IDEAS
- Janela de X operações
- No contexto do EUNOMIA, um nó deve estar sempre ligado à rede - se o nó está offline, assumimos que o que fizer não conta
- index +- `x/2`
- maneira de ter indice mais recente por consenso (blockchain)
- nós maliciosos - tratar disso mais tarde

- Change `join` method from `ipfs-log` to return the new entries
  - Or it could say in which position of the log the new entries changes were placed

- `join` method allows for giving a limit (`size`) for the size of the returned log
  - Using this might not be ideal because then all nodes will have a small log at all times, not keeping history (?)
  - But all log entries should actually be stored in IPFS regardless of the size given to `join`
  - Can we maybe use this, but, before calling `join`, save the current log in a `logHistory` array?

- __IMPORTANT__ `logFromIndexSnapshot`: create the base log from the Index snapshot. If there is an operation on an object that is already in the Index, we must know if this operation was supposed to happen before or after the current state.

## TODO
- Fazer teste com log de várias heads
  - Create two instances of the same DB
  - Add stuff to both at the same time (`put` without `async`)
  - Then create a third instance and let replicate

- Olhar para os nexts na app que fiz para experimentar o ipfs-log
  - A ideia é perceber como está ligado o log, especialmente depois de joins

- Create an orbitDB instance from an index object
  - Then get that index from IPFS
  - Think about proper serialization

- Set a threshold for number of operations to "look back" in the log after a join/replicate
  - How can I do this?
    - Try to understand the replicate process very well -- replicate events, `_onMessage`, `_onPeerConnect`, and so on


- Would it be easy/possible to adapt the `saveSnapshot` and `loadFromSnapshot` methods to use the `index` instead of the `oplog`?
- Is the `index` stored on IPFS? Or is it always only in memory? Is this a relevant question?
- Take a better look at `saveSnapshot` and `loadFromSnapshot`
- Look at joining logs after remote update and how arriving operations are executed
- Take closer look at [issue](https://github.com/orbitdb/ipfs-log/issues/136) and [pull request](https://github.com/orbitdb/ipfs-log/pull/301) about reducing memory usage
- Understand what the `cache` object is and what it does...

- `updateIndex` could be updated to not go through the whole log but instead look only at the new log entries
  - Does the same process but breaks the loop once all new entries have been visited
  - Goes backwards in the log and keeps track of the new log entries which have been processed
- Maybe we do not need a complicated/very accurate synchronization mechanism to guarantee that node take snapshots of the same DB state all around the network. A new node just needs to ask one (or maybe more to resist against malicious behavior) of the network's nodes for their state and then behave normally from there.
  - Ask `k` nodes for their snapshot state (index)
  - Merge the `k` snapshots
  - (Get same updates as the `k` nodes until _something_)

## Issues
- The `updateIndex` method
  - Every time the database's `put` method is called, the index's `updateIndex` method is also called. This method goes through all the entries in the `oplog` in reverse order and updates the index's state with the information in the most recent entry for every object.
- Using `putAll` and then `get` (from `orbit-db-docstore`) does not work! The index entries are generated incorrectly for the docs passed in the `putAll` call. Afterwards, the `get` method does not find the `payload` property and it crashes.
