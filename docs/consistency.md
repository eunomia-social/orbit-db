# Implications on eventual consistency

When introducing snapshots and, most importantly, limiting the replication level of the operation log, we might be affecting the consistency provided by the system. In certain scenarios, inconsistencies will arise, as we are essentially trading consistency for efficiency. We try to list some of these scenarios, and how to tackle them - either how to fix them or understand which assumptions are necessary to eliminate them.


## Local and Remote heads
(see test file `consistency.test.js` in our `orbit-db` repository)

A problem I identified recently is related to the fact that nodes keep two different sets of heads which are then used for replication. These sets contain the _local_ and _remote_ heads.

___Local heads:___ this is the latest change the node made __locally__ to the database. When such a change occurs, the entry representing this change becomes the head of the log.
(Even though it is referred to as plural _heads_, this should always be a single entry)

___Remote heads:___ these are the heads of the log after the most recent remote update. I.e., the heads resulting from the last update to the log with changes coming from the network. In other words, _remote heads_ has the set of heads of the log after the `load.end` event is emitted - which happens when there are __new entries__ to append to the log __that came from the network__. These new entries from the network are added to the log, and _remote heads_ is filled with the current heads of the log. This means that _remote heads_ can include entries that were generated locally -- the update of _remote heads_ is triggered by remote updates, but a local change could still be one of the heads of the log, and hence, would be included in _remote heads_.

[take a look at [this file](./orbit-manual.md) if this is not yet clear]

What affects our system it the fact that, when a new peer joins, __both remote and local heads are sent to the node__. I initially though only the actual log heads were sent to the new node, but this makes the behavior of the system slightly more complex to follow.

Here is an example where this affects consistency with our current construction.

Let us have 3 nodes, where the first sets up the database and the second node joins right afterwards. The third remains inactive until later.

```txt

  -------NODE 1-------    -------NODE 2-------
        startup
                                startup
                   <--sync-->

     write A:=0 (t0)         write B:=0 (t0)

                   <--sync-->

   -heads-                 -heads-
   local: {A=0}            local: {B=0}
   remote: {A=0;B=0}       remote: {A=0;B=0}

   -log-------------------------------------------- same on node 1 & 2
    [A=0] [B=0]                                     [heads in brackets]

   -index------------------------------------------ same on node 1 & 2
    {A:0, B:0}

                             write C:=1 (t1)
                             write B:=2 (t2)
                             write C:=2 (t3)
                             write C:=3 (t4)
                             write C:=4 (t5)

                   <--sync-->

   -heads-                 -heads-
   local: {A=0}            local: {C=4}
   remote: {C=4}           remote: {A=0;B=0}

   -log-------------------------------------------- same on node 1 & 2
    A=0 <-- C=1 <-- B=2 <-- C=2 <-- C=3 <-- [C=4]   [head in brackets]
             |
    B=0 <----+

   -index------------------------------------------ same on node 1 & 2
   {A:0, B:2, C:4}



                                                  -------NODE 3-------

                                                     joins network

                          sends snapshot
                              --- {A:0, B:2, C:4} --->
                          and heads
                              --- {C=4; A=0; B=0} --->
     sends heads
                ------------ {A=0; C=4} ------------->
                                                 instantiates DB from
                                                 snapshot and replicates
                                                 log w/ depth 3


                                                 this results in the log:

                                                 [A=0] [B=0] C=2<--C=3<--[C=4]
                                                 the heads are in brackets

                                                 After replicating the log,
                                                 the index is updated, which
                                                 results in DB state:

                                                 {A:0, B:0, C:4}

                                                 with applyIndex, the log is:
                                                 [A=0] [B=0] [B=2] [C=4]

                                                 and the state:
                                                 {A:0, B:2, C:4}

```

As you can see, even though the snapshot holds the correct state (namely with `B=2`), Node 3 converges to an incorrect state.

When updating the index, the log is traversed and each object is updated according to the last entry that refers it. In this scenario, the only log entry on Node 3 referencing object `B` is the one with write `B:=0`. This prevails over the information present in the index snapshot. Indeed, the node does not have enough information to decide which is the latest state of object `B`.

Because of the replication depth, the entry in the log for the write `B:=2` is not replicated on Node 3, and there is no guarantee that it will ever happen (even eventually).

### Solution
To solve this issue, our solution is to introduce the log entries referring to the current state of the database in the node's log on startup (applyIndex).

Steps:
- Receive index snapshot
- Add index entries to the operation log
- Replicate the rest of the log as usual (according to max replication depth)

___Note:___ since most of the heads a node will receive from the network will already be in the log (as they are added with applyIndex before replication begins), the replication process will often do nothing - when the log already contains an entry it is trying to add, that branch of the replication is abandoned. Thus, __it might be possible to drop the replication depth feature without losing our intended performance improvement__.


## Old update from offline node

Node 1 and 2 connect

Node 1 goes offline
Node 2 does a bunch of writes
Node 3 joins and gets snapshot and heads from node 2

Node 1 writes existing object and comes back online

After node 1 sends the update to the network, what will be the state on node 3?

### Solution
This should not affect consistency as long as we use the solution from the above scenario (updateIndex). This would pose a problem if Node 3 did not have all the log entries that correspond to the state. In that case, it might wrongly update the index, by considering as new an update that is already old.


## Index snapshot has old state

If the snapshot does not contain the latest state of the database, we should be able to converge to it.

### Solution
The solution here is guaranteeing/assuming the difference between the latest snapshot and the latest DB state is smaller than the replication depth limit (replication window).


## More ideas?
Please add more scenarios here.


---
If you want to read further, take a look at [this file](./orbit-snapshots.md). It is quite rough, incomplete, and not too reader friendly, but it might give some interesting insight into OrbitDB replication and our solution.

