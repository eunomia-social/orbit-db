// result of calling `db.saveSnapshot()` - stored on IPFS
{
	"id":"/orbitdb/zdpuApZb349k1WJzC34m17UmwjKCrSc7pK9zWzBcoMBwy1Hoy/orbit-db-tests",
	"heads":[{"hash":"zdpuAkuDUDGchEQcBmKiypCoBSxRcsVtCmoVrJ28HbyKRTLQ5","id":"/orbitdb/zdpuApZb349k1WJzC34m17UmwjKCrSc7pK9zWzBcoMBwy1Hoy/orbit-db-tests","payload":{"op":"PUT","key":"hello world","value":{"_id":"hello world","doc":"none of the things are things"}},"next":["zdpuAqbkj5s7hP4NXTQAf5L3eNYBjrBrm5NsmnigGegEtvWp5"],"refs":["zdpuAq9VqZv9zA4jc9CTaBHiB4snhdN6YTV5pmyZLfwrt6DjE","zdpuB29FfD284QbcdtVjRsBc29xbpjMnopT4MvHSFaXoKoNCA"],"v":2,"clock":{"id":"04eee29c5452723f74336a7a367041b760b87d2009f15225f08e68e61a2cfaf21ea8a9efc56e7697e703fac5145bfd7920708ca738503acdc35e84483667e23f3e","time":4},"key":"04eee29c5452723f74336a7a367041b760b87d2009f15225f08e68e61a2cfaf21ea8a9efc56e7697e703fac5145bfd7920708ca738503acdc35e84483667e23f3e","identity":{"id":"0276bc08bb415fbdb4c546e79e05bfb3b2f149b1ac5af05ac61216abb5aeeb84ad","publicKey":"04eee29c5452723f74336a7a367041b760b87d2009f15225f08e68e61a2cfaf21ea8a9efc56e7697e703fac5145bfd7920708ca738503acdc35e84483667e23f3e","signatures":{"id":"304402200303737c3635c0cb3816ed6349980c5bd97b314f04681e88f25380d3449f5c5002200ac92afa80ffa385b947f10e9f8acb535670b0f332b5d2867162a37234c711da","publicKey":"3045022100acacc19df8370a30326f19af79bdcdfe82d06f3779bd9bfc828156ce91c9987b022048f6b8704f54e147b6a3a0a854bbb06c8b013176145473564e5757fa42e74853"},"type":"orbitdb"},"sig":"304402203ce405a9bb759a70dc64ddd670a3e946bf21e3ae7b3b3e5198ea8e126309778802202702da6a48c701a47f3a93fd5555b8046f6280e9759309ff5f24658c412d472e"}],
	"size":4,
	"type":"docstore"
}
{
	"hash":"zdpuB29FfD284QbcdtVjRsBc29xbpjMnopT4MvHSFaXoKoNCA",
	"id":"/orbitdb/zdpuApZb349k1WJzC34m17UmwjKCrSc7pK9zWzBcoMBwy1Hoy/orbit-db-tests",
	"payload":
		{
			"op":"PUT",
			"key":"hello world",
			"value":{"_id":"hello world","doc":"all the things"}
		},
	"next":[],
	"refs":[],
	"v":2,
	"clock":{"id":"04eee29c5452723f74336a7a367041b760b87d2009f15225f08e68e61a2cfaf21ea8a9efc56e7697e703fac5145bfd7920708ca738503acdc35e84483667e23f3e","time":1},
	"key":"04eee29c5452723f74336a7a367041b760b87d2009f15225f08e68e61a2cfaf21ea8a9efc56e7697e703fac5145bfd7920708ca738503acdc35e84483667e23f3e",
	"identity":{"id":"0276bc08bb415fbdb4c546e79e05bfb3b2f149b1ac5af05ac61216abb5aeeb84ad","publicKey":"04eee29c5452723f74336a7a367041b760b87d2009f15225f08e68e61a2cfaf21ea8a9efc56e7697e703fac5145bfd7920708ca738503acdc35e84483667e23f3e","signatures":{"id":"304402200303737c3635c0cb3816ed6349980c5bd97b314f04681e88f25380d3449f5c5002200ac92afa80ffa385b947f10e9f8acb535670b0f332b5d2867162a37234c711da","publicKey":"3045022100acacc19df8370a30326f19af79bdcdfe82d06f3779bd9bfc828156ce91c9987b022048f6b8704f54e147b6a3a0a854bbb06c8b013176145473564e5757fa42e74853"},"type":"orbitdb"},
	"sig":"3045022100d9e8b2c50dec7c8d762a969af0e020b059472cc3780ac533aacfd802cd70580502201ebc40983919a3055d89805e3ee80f2ea78090b3df2eff9051e6e0877b2bc1cc"
}
{
	"hash":"zdpuAq9VqZv9zA4jc9CTaBHiB4snhdN6YTV5pmyZLfwrt6DjE",
	"id":"/orbitdb/zdpuApZb349k1WJzC34m17UmwjKCrSc7pK9zWzBcoMBwy1Hoy/orbit-db-tests",
	"payload":
		{
			"op":"PUT",
			"key":"hello world 2",
			"value":{"_id":"hello world 2","doc":"all the things"}
		},
	"next":["zdpuB29FfD284QbcdtVjRsBc29xbpjMnopT4MvHSFaXoKoNCA"],
	"refs":[],
	"v":2,
	"clock":{"id":"04eee29c5452723f74336a7a367041b760b87d2009f15225f08e68e61a2cfaf21ea8a9efc56e7697e703fac5145bfd7920708ca738503acdc35e84483667e23f3e","time":2},
	"key":"04eee29c5452723f74336a7a367041b760b87d2009f15225f08e68e61a2cfaf21ea8a9efc56e7697e703fac5145bfd7920708ca738503acdc35e84483667e23f3e",
	"identity":{"id":"0276bc08bb415fbdb4c546e79e05bfb3b2f149b1ac5af05ac61216abb5aeeb84ad","publicKey":"04eee29c5452723f74336a7a367041b760b87d2009f15225f08e68e61a2cfaf21ea8a9efc56e7697e703fac5145bfd7920708ca738503acdc35e84483667e23f3e","signatures":{"id":"304402200303737c3635c0cb3816ed6349980c5bd97b314f04681e88f25380d3449f5c5002200ac92afa80ffa385b947f10e9f8acb535670b0f332b5d2867162a37234c711da","publicKey":"3045022100acacc19df8370a30326f19af79bdcdfe82d06f3779bd9bfc828156ce91c9987b022048f6b8704f54e147b6a3a0a854bbb06c8b013176145473564e5757fa42e74853"},"type":"orbitdb"},
	"sig":"3045022100c12dc52cf192820fdcd4face3ad6c4e8ee6ab7c7405eca350e474748f18f22fa02206c1fa5c7f7b91f904e41694b4010c01455efc878b3d0f692a555fb057c6706b4"
}
{
	"hash":"zdpuAqbkj5s7hP4NXTQAf5L3eNYBjrBrm5NsmnigGegEtvWp5",
	"id":"/orbitdb/zdpuApZb349k1WJzC34m17UmwjKCrSc7pK9zWzBcoMBwy1Hoy/orbit-db-tests",
	"payload":
		{
			"op":"PUT",
			"key":"sup world",
			"value":{"_id":"sup world","doc":"other things"}
		},
	"next":["zdpuAq9VqZv9zA4jc9CTaBHiB4snhdN6YTV5pmyZLfwrt6DjE"],
	"refs":["zdpuB29FfD284QbcdtVjRsBc29xbpjMnopT4MvHSFaXoKoNCA"],
	"v":2,
	"clock":{"id":"04eee29c5452723f74336a7a367041b760b87d2009f15225f08e68e61a2cfaf21ea8a9efc56e7697e703fac5145bfd7920708ca738503acdc35e84483667e23f3e","time":3},
	"key":"04eee29c5452723f74336a7a367041b760b87d2009f15225f08e68e61a2cfaf21ea8a9efc56e7697e703fac5145bfd7920708ca738503acdc35e84483667e23f3e",
	"identity":{"id":"0276bc08bb415fbdb4c546e79e05bfb3b2f149b1ac5af05ac61216abb5aeeb84ad","publicKey":"04eee29c5452723f74336a7a367041b760b87d2009f15225f08e68e61a2cfaf21ea8a9efc56e7697e703fac5145bfd7920708ca738503acdc35e84483667e23f3e","signatures":{"id":"304402200303737c3635c0cb3816ed6349980c5bd97b314f04681e88f25380d3449f5c5002200ac92afa80ffa385b947f10e9f8acb535670b0f332b5d2867162a37234c711da","publicKey":"3045022100acacc19df8370a30326f19af79bdcdfe82d06f3779bd9bfc828156ce91c9987b022048f6b8704f54e147b6a3a0a854bbb06c8b013176145473564e5757fa42e74853"},"type":"orbitdb"},
	"sig":"3044022044be1ac3f71cc7811ea16b2b97d8d66a5fd15b86d7f9717efea51fad48922a9602203e09b7d6a7d7e74edf350196c2522d6d19168bbc9938647c23aa0b10dc66e1e8"
}
{
	"hash":"zdpuAkuDUDGchEQcBmKiypCoBSxRcsVtCmoVrJ28HbyKRTLQ5",
	"id":"/orbitdb/zdpuApZb349k1WJzC34m17UmwjKCrSc7pK9zWzBcoMBwy1Hoy/orbit-db-tests",
	"payload":
		{
			"op":"PUT",
			"key":"hello world",
			"value":{"_id":"hello world","doc":"none of the things are things"}
		},
	"next":["zdpuAqbkj5s7hP4NXTQAf5L3eNYBjrBrm5NsmnigGegEtvWp5"],
	"refs":["zdpuAq9VqZv9zA4jc9CTaBHiB4snhdN6YTV5pmyZLfwrt6DjE","zdpuB29FfD284QbcdtVjRsBc29xbpjMnopT4MvHSFaXoKoNCA"],
	"v":2,
	"clock":{"id":"04eee29c5452723f74336a7a367041b760b87d2009f15225f08e68e61a2cfaf21ea8a9efc56e7697e703fac5145bfd7920708ca738503acdc35e84483667e23f3e","time":4},
	"key":"04eee29c5452723f74336a7a367041b760b87d2009f15225f08e68e61a2cfaf21ea8a9efc56e7697e703fac5145bfd7920708ca738503acdc35e84483667e23f3e",
	"identity":{"id":"0276bc08bb415fbdb4c546e79e05bfb3b2f149b1ac5af05ac61216abb5aeeb84ad","publicKey":"04eee29c5452723f74336a7a367041b760b87d2009f15225f08e68e61a2cfaf21ea8a9efc56e7697e703fac5145bfd7920708ca738503acdc35e84483667e23f3e","signatures":{"id":"304402200303737c3635c0cb3816ed6349980c5bd97b314f04681e88f25380d3449f5c5002200ac92afa80ffa385b947f10e9f8acb535670b0f332b5d2867162a37234c711da","publicKey":"3045022100acacc19df8370a30326f19af79bdcdfe82d06f3779bd9bfc828156ce91c9987b022048f6b8704f54e147b6a3a0a854bbb06c8b013176145473564e5757fa42e74853"},"type":"orbitdb"},
	"sig":"304402203ce405a9bb759a70dc64ddd670a3e946bf21e3ae7b3b3e5198ea8e126309778802202702da6a48c701a47f3a93fd5555b8046f6280e9759309ff5f24658c412d472e"
}
