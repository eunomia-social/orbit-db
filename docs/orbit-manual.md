# OrbitDB

This document tries to briefly explain how OrbitDB works, focusing on the necessary concepts to understand the replication process.

## ipfs-log (CRDT)

The mechanism used in OrbitDB to guarantee eventual consistency is a [CRDT](https://en.wikipedia.org/wiki/Conflict-free_replicated_data_type) similar to a grow-only set, and is called [ipfs-log](https://github.com/orbitdb/ipfs-log/). It is an append-only log where every entry in the log is saved in IPFS and each points to a hash of previous entry(ies) forming a directed tree graph. Logs can be forked and joined back together.

A new entry is added as a _head_ - node with no incoming edges - and connected to the existing roots - the new edges are directed from the new root to the old ones.

The following diagrams illustrate the addition of entries to the log.
```txt
State of the log:
              +----+       +----+
              | A1 +<------+ A2 | Head
              +----+       +----+
 
 +----+       +----+       +----+
 | B1 +<------+ B2 +<------+ B3 | Head
 +----+       +----+       +----+


Add an entry (C1):
 
              +----+       +----+
              | A1 +<------+ A2 +<------+
              +----+       +----+       |
                                      +-+--+
                                      | C1 | Head
                                      +-+--+
 +----+       +----+       +----+       |
 | B1 +<------+ B2 +<------+ B3 +<------+
 +----+       +----+       +----+


Add another entry (C2):
 
              +----+       +----+
              | A1 +<------+ A2 +<------+
              +----+       +----+       |
                                      +-+--+       +----+
                                      | C1 +<------+ C2 | Head
                                      +-+--+       +----+
 +----+       +----+       +----+       |
 | B1 +<------+ B2 +<------+ B3 +<------+
 +----+       +----+       +----+

```

The most important properties of an entry are:
- `hash`: a unique identifier for the entry computed over its contents
- `next`: an array of hashes that identify the entries which the current entry is connected to
- `payload`: the actual payload data the entry represents
- `clock`: value that permits the ordering of the entries

The eventual consistency model expects every node to keep a local copy of the log which is not necessarily the most up to date version. The requirement is that when new entries arrive from the network, they are ordered in a deterministic way, so that for the same set of entries, all nodes see them in the same order. This ordering is achieved through a Lamport clock - a counter which is incremented every time a new entry is added to the log. The conflicts that arise from concurrent entry creation are solved using the ID of the node - the entry created by the node with the highest ID is considered the latest of the two entries.

So in order to view this log, which is a tree, as an actual log (an ordered list of entries), all the entries are ordered. If we take the example above - consider the letter is the ID of the node, and the number is its Lamport clock - the ordering would be: A1, B1, A2, B2, B3, C1, C2.


### ipfs-log for databases

This log is used in OrbitDB to keep track of changes to the database. Each entry represents a change in the database - either a write or delete - storing in its payload which object it refers to. When querying the state of the database, the latest entry referencing an object contains what is considered the current state of the object. The set of those entries, the latest for each object, represents the current state of the database and is kept in the _index_ structure.


## Updating the state

Updating the state of the database can happen because of two events: either the node makes a change locally (local update), or it receives an update from the network (remote update).

### Local Update

When a local update is made, a new entry is added to the log and it becomes its head. This entry is saved in the local cache as the _local head_.

### Remote Update

When an update arrives from the network (through the pubsub topic), it is joined with the current local log and the heads of the log are recomputed. The resulting value is stored in the local cache as the _remote heads_.

### Update Index

After an update, the index structure is updated. This is done by traversing the log in order from the latest entry to the oldest, and for every entry:
- if the object it references has not been seen before during this update, the object's state is set according to this entry
- if the object was seen before during this update (previous step), all entries referencing it are ignored


## Replication

The replication process starts when a new node joins the network. This node subscribes the database's pubsub topic and then the other nodes in the network are notified that a node joined.

The network nodes send the new node their heads (an array containing both local and remote heads). The joining node then starts reconstructing the log from the received heads. As entries are added to the log, the node's index is also updated.


## Improving join performance

As the log grows large quickly and often includes information that goes way beyond the current state of the database (it includes the whole operation history), we realize that we do not need it whole when a new node joins.

To address this issue we introduced a limit on how much of the log is replicated by a joining node. Additionally, we enabled a possibility to instantiate a node from an index snapshot - so that even though we may not have the whole log, we will still have the whole state of the database.

With our solution, the whole process remains unchanged with two changes:
- a new node takes as input a snapshot of the index, which was created by another node that is already part of the network
- while replicating the log, when a certain depth of the (log) tree has been reached, the process stops - this results in a partial replication of the log



---
Now take a look at the [implications on consistency](./consistency.md) our solution might introduce.
