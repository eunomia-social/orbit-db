'use strict'

const assert = require('assert')
const rmrf = require('rimraf')
const OrbitDB = require('../src/OrbitDB')

// Include test utilities
const {
  config,
  startIpfs,
  stopIpfs,
  connectPeers,
  waitForPeers,
  testAPIs,
} = require('orbit-db-test-utils')

const dbPath1 = './orbitdb/tests/multiple-databases/1'
const dbPath2 = './orbitdb/tests/multiple-databases/2'
const ipfsPath1 = './orbitdb/tests/multiple-databases/1/ipfs'
const ipfsPath2 = './orbitdb/tests/multiple-databases/2/ipfs'

const dbInterface = {
    name: 'documents',
    open: async (orbitdb, address, options) => await orbitdb.docs(address, options),
  }

Object.keys(testAPIs).forEach(API => {
  describe(`orbit-db - Empty Snapshots (${API})`, function() {
    this.timeout(config.timeout)

    let ipfsd1, ipfsd2, ipfs1, ipfs2
    let orbitdb1, orbitdb2

    let localDatabase, remoteDatabase

    let numberOfEntries

    // Create two IPFS instances and two OrbitDB instances (2 nodes/peers)
    before(async () => {
      config.daemon1.repo = ipfsPath1
      config.daemon2.repo = ipfsPath2
      rmrf.sync(config.daemon1.repo)
      rmrf.sync(config.daemon2.repo)
      rmrf.sync(dbPath1)
      rmrf.sync(dbPath2)
      ipfsd1 = await startIpfs(API, config.daemon1)
      ipfsd2 = await startIpfs(API, config.daemon2)
      ipfs1 = ipfsd1.api
      ipfs2 = ipfsd2.api
      // Connect the peers manually to speed up test times
      await connectPeers(ipfs1, ipfs2)
      orbitdb1 = await OrbitDB.createInstance(ipfs1, { directory: dbPath1 })
      orbitdb2 = await OrbitDB.createInstance(ipfs2, { directory: dbPath2 })

      // Open the database on the first node
      const options = { create: true, accessController: { write: ['*'] } }
      localDatabase = await dbInterface.open(orbitdb1, dbInterface.name, options)

      // Add some docs to the local DB before remote node joins
      numberOfEntries = 8
      for (var i = 1; i <= numberOfEntries; i++) {
        await localDatabase.put({ _id: `hello${i}`, testing: i })
      }

      // Make sure all documents were inserted in the DB
      assert.equal(Object.values(localDatabase.index).length, numberOfEntries)
    })

    after(async () => {
      if (localDatabase) await localDatabase.drop()
      if (remoteDatabase) await remoteDatabase.drop()

      if(orbitdb1)
        await orbitdb1.stop()

      if(orbitdb2)
        await orbitdb2.stop()

      if (ipfsd1)
        await stopIpfs(ipfsd1)

      if (ipfsd2)
        await stopIpfs(ipfsd2)
    })

    it('instantiates a remote docstore from an empty Index snapshot', async () => {
      // Delete entries before taking snapshot
      for (var i = 1; i <= numberOfEntries; i++) {
        await localDatabase.del(`hello${i}`)
      }

      // Make sure all documents were deleted from the DB
      assert.equal(Object.values(localDatabase.index).length, 0)
      assert.equal(localDatabase._oplog.length, numberOfEntries*2)

      // Take snapshot
      await localDatabase.saveIndexSnapshotToIPFS()

      // Open the DB on the second node, using the index snapshot (we say only that
      // that we want to initialize from a snapshot, and give no more info on it)
      // The second peer fetches the snapshot from the network and uses it to
      // initialize its index
      const options = { sync: true, indexSnapshot: true }
      const address = localDatabase.address.toString()
      remoteDatabase = await dbInterface.open(orbitdb2, address, options)

      // Wait for remote node to initialize index from snapshot
      await new Promise((resolve) => {
        const interval = setInterval(() => {
          if (remoteDatabase._oplog.length >= 1) {
            clearInterval(interval)
            resolve()
          }
        }, 500)
      })

      // Without replication happening, see if remote index is the same as local index
      assert.equal(Object.values(localDatabase.index).length, Object.values(remoteDatabase.index).length)
      assert.deepStrictEqual(localDatabase.index, remoteDatabase.index)
      assert.equal(Object.values(remoteDatabase.index).length, 0)
    })

    it('add one update to local db and remote replicates only new update', async () => {
      // Add object to local database
      await localDatabase.put({ _id: `hellyeah`, testing: 125 })

      // Make sure document was inserted in local DB
      assert.equal(Object.values(localDatabase.index).length, 1)
      assert.equal(localDatabase._oplog.length, numberOfEntries*2+1)
      const objs = await localDatabase.get('hellyeah')
      assert.equal(objs.length, 1)
      assert.deepStrictEqual(objs[0], { _id: `hellyeah`, testing: 125 })

      // Wait for remote node to initialize index from snapshot
      await new Promise((resolve) => {
        const interval = setInterval(() => {
          if (Object.values(remoteDatabase.index).length >= 1) {
            clearInterval(interval)
            resolve()
          }
        }, 500)
      })

      // Make sure document was replicated by remote DB
      assert.equal(Object.values(remoteDatabase.index).length, 1)
      assert.equal(remoteDatabase._oplog.length, 2)
      const remoteObjs = await remoteDatabase.get('hellyeah')
      assert.equal(remoteObjs.length, 1)
      assert.deepStrictEqual(remoteObjs[0], { _id: `hellyeah`, testing: 125 })
    })
  })
})
