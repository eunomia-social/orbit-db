'use strict'

const assert = require('assert')
const mapSeries = require('p-each-series')
const rmrf = require('rimraf')
const OrbitDB = require('../src/OrbitDB')

// Include test utilities
const {
  config,
  startIpfs,
  stopIpfs,
  connectPeers,
  waitForPeers,
  testAPIs,
} = require('orbit-db-test-utils')

const dbPath1 = './orbitdb/tests/multiple-databases/1'
const dbPath2 = './orbitdb/tests/multiple-databases/2'
const ipfsPath1 = './orbitdb/tests/multiple-databases/1/ipfs'
const ipfsPath2 = './orbitdb/tests/multiple-databases/2/ipfs'

const dbInterface = {
    name: 'documents',
    open: async (orbitdb, address, options) => await orbitdb.docs(address, options),
    write: async (db, index) => await db.put({ _id: `hello${index}`, testing: index }),
    query: (db) => {
      const docs = db.get('hello')
      return docs ? docs[0].testing : 0
    }
  }

const sleep = ms => new Promise((resolve) => {
    setTimeout(resolve, ms);
  })

const logHas = (log, key) => log.values.reduce((res, e) => res || e.payload.key === key, false)

const disconnectPeers = async (ipfs1, ipfs2) => {
  const id1 = await ipfs1.id()
  const id2 = await ipfs2.id()
  await ipfs1.swarm.disconnect(id2.addresses[0])
  await ipfs2.swarm.disconnect(id1.addresses[0])
}

const waitForIndex = async (db, size, { time = 500 } = {}) => {
  await new Promise((resolve) => {
    const interval = setInterval(() => {
      if (Object.keys(db.index).length >= size) {
        clearInterval(interval)
        resolve()
      }
    }, time)
  })
}

Object.keys(testAPIs).forEach(API => {
  describe(`orbit-db - Replication depth limit (${API})`, function() {
    this.timeout(config.timeout)

    let ipfsd1, ipfsd2, ipfs1, ipfs2
    let orbitdb1, orbitdb2

    let database1, database2

    const startIPFS1 = async () => {
      config.daemon1.repo = ipfsPath1
      rmrf.sync(config.daemon1.repo)
      rmrf.sync(dbPath1)
      ipfsd1 = await startIpfs(API, config.daemon1)
      ipfs1 = ipfsd1.api
    }

    const startIPFS2 = async () => {
      config.daemon2.repo = ipfsPath2
      rmrf.sync(config.daemon2.repo)
      rmrf.sync(dbPath2)
      ipfsd2 = await startIpfs(API, config.daemon2)
      ipfs2 = ipfsd2.api
    }

    const stopNode1 = async () => {
      if(orbitdb1)
        await orbitdb1.stop()

      if (ipfsd1)
        await stopIpfs(ipfsd1)
    }

    const stopNode2 = async () => {
      if(orbitdb2)
        await orbitdb2.stop()

      if (ipfsd2)
        await stopIpfs(ipfsd2)
    }

    // TODO: remove and add actual 3rd node setup (like in apply-index-to-log.test.js)
    const restartNode2 = async (options) => {
      await database2.drop()

      await stopNode2()

      await startIPFS2()
      // Connect the peers manually to speed up test times
      await connectPeers(ipfs1, ipfs2)

      orbitdb2 = await OrbitDB.createInstance(ipfs2, { directory: dbPath2 })

      // Reopen the DB on the second node
      // The second peer fetches the db manifest from the network
      options = Object.assign({}, options, { sync: true })
      const address = database1.address.toString()
      database2 = await dbInterface.open(orbitdb2, address, options)
    }

    // Create two IPFS instances and two OrbitDB instances (2 nodes/peers)
    beforeEach(async () => {
      await startIPFS1()
      await startIPFS2()
      // Connect the peers manually to speed up test times
      await connectPeers(ipfs1, ipfs2)
      orbitdb1 = await OrbitDB.createInstance(ipfs1, { directory: dbPath1 })
      orbitdb2 = await OrbitDB.createInstance(ipfs2, { directory: dbPath2 })

      // Open the database on the first node
      const options = { create: true, accessController: { write: ['*'] } }
      database1 = await dbInterface.open(orbitdb1, dbInterface.name, options)
    })

    afterEach(async () => {
      await database1.drop()
      await database2.drop()

      await stopNode1()
      await stopNode2()
    })

    describe('List log structure', async () => {
      beforeEach(async () => {
        // Add some docs to the local DB before remote node joins
        await dbInterface.write(database1, 1)
        await dbInterface.write(database1, 2)
        await dbInterface.write(database1, 3)
        await dbInterface.write(database1, 4)

        // Make sure all documents were inserted in the DB
        assert.equal(Object.values(database1.index).length, 4)
        assert.equal(database1._oplog.length, 4)
      })

      it('should partially (according to limit) replicate the log and state', async () => {
        // Open the DB on the second node, using 3 as the maximum replication
        // depth. The second peer fetches the db manifest from the network
        const options = { sync: true, maxReplicationDepth: 3 }
        const address = database1.address.toString()
        database2 = await dbInterface.open(orbitdb2, address, options)

        // Give enough time for replication to happen
        await sleep(2000)

        assert.equal(database1._oplog.length, 4)
        assert.equal(Object.keys(database1.index).length, 4)

        assert.equal(database2._oplog.length, 3)
        assert.equal(Object.keys(database2.index).length, 3)

        assert.equal(database1.get('hello1').length, 1)
        assert.equal(database2.get('hello1').length, 0)
      })

      it('should successfully update log after partial replication', async () => {
        // Open the DB on the second node, using 3 as the maximum replication
        // depth. The second peer fetches the db manifest from the network
        const options = { sync: true, maxReplicationDepth: 3 }
        const address = database1.address.toString()
        database2 = await dbInterface.open(orbitdb2, address, options)

        // Give enough time for replication to happen
        await sleep(2000)

        assert.equal(database1._oplog.length, 4)
        assert.equal(Object.keys(database1.index).length, 4)
        assert.equal(database2._oplog.length, 3)
        assert.equal(Object.keys(database2.index).length, 3)
        assert.equal(database1.get('hello1').length, 1)
        assert.equal(database2.get('hello1').length, 0)

        await dbInterface.write(database2, 22)

        await sleep(500)

        assert.equal(database2._oplog.length, 4)
        assert.equal(Object.keys(database2.index).length, 4)
        assert.equal(database2._oplog.heads.length, 1)

        const nextHash = database2._oplog.heads[0].next[0]
        const next = database2._oplog.get(nextHash)
        assert.equal(next.payload.key, 'hello4')
      })
    })

    describe('Tree log structure', () => {
      beforeEach(async () => {
        // Open the DB on the second node
        // The second peer fetches the db manifest from the network
        const options = { sync: true }
        const address = database1.address.toString()
        database2 = await dbInterface.open(orbitdb2, address, options)

        // Add some docs concurrently to the databases
        dbInterface.write(database1, '1A')
        dbInterface.write(database1, '1B')
        dbInterface.write(database2, '2A')
        dbInterface.write(database2, '2B')

        // Wait for both nodes to replicate each other and join
        // the logs, before adding more documents
        await sleep(1000)

        // Add more documents after logs have been joined
        dbInterface.write(database1, '1C')
        dbInterface.write(database1, '1D')
        dbInterface.write(database2, '2C')
        dbInterface.write(database2, '2D')

        // Wait for replication to finish
        await sleep(1000)

        // The above produces the following log structure:
        //
        //  [latest entries]
        //    -----------
        //   |  2D   1D  |    depth 1
        //   |   |   |   |
        //   |   v   v   |
        //   |  2C   1C  |    depth 2
        //   |   |\ /|   |
        //   |   | X |   |
        //   |   |/ \|   |
        //   |   v   v   |
        //   |  2B   1B  |    depth 3
        //   |   |   |   |
        //   |   v   v   |
        //   |  2A   1A  |    depth 4
        //    -----------
        //  [older entries]
        //
        // (The arrows represent the `next` connections)

        // Assert that both nodes have the same state
        assert.equal(database1._oplog.length, 8)
        assert.equal(database2._oplog.length, 8)
        assert.equal(database1.get('hello').length, 8)
        assert.equal(database2.get('hello').length, 8)
        assert.equal(database1.get('hello1').length, 4)
        assert.equal(database1.get('hello2').length, 4)
        assert.equal(database2.get('hello1').length, 4)
        assert.equal(database2.get('hello2').length, 4)
        for (const l of ['A', 'B', 'C', 'D']) {
          assert.equal(database1.get(`hello1${l}`).length, 1)
          assert.equal(database1.get(`hello2${l}`).length, 1)
          assert.equal(database2.get(`hello1${l}`).length, 1)
          assert.equal(database2.get(`hello2${l}`).length, 1)
        }
      })

      it('should partially (according to limit) replicate the log and state', async () => {
        // Restart node 2 to simulate a third node
        // Using a maxReplicationDepth of 3 should replicate all log entries until
        // depth 3 - i.e., it should not replicate entries 1A and 2A
        await restartNode2({ maxReplicationDepth: 3 })

        // Wait for restarted node to finish replication
        await sleep(2000)

        assert.equal(database2._oplog.length, 6)
        assert.equal(Object.keys(database2.index).length, 6)

        assert.equal(database2.get('hello1A').length, 0)
        assert.equal(database2.get('hello2A').length, 0)

        assert.equal(database2.get('hello1B').length, 1)
        assert.equal(database2.get('hello1C').length, 1)
        assert.equal(database2.get('hello1D').length, 1)
        assert.equal(database2.get('hello2B').length, 1)
        assert.equal(database2.get('hello2C').length, 1)
        assert.equal(database2.get('hello2D').length, 1)
      })

      it('should successfully update log after partial replication', async () => {
        // Restart node 2 to simulate a third node
        // Using a maxReplicationDepth of 3 should replicate all log entries until
        // depth 3 - i.e., it should not replicate entries 1A and 2A
        await restartNode2({ maxReplicationDepth: 3 })

        // Wait for restarted node to finish replication
        await sleep(2000)

        assert.equal(database2._oplog.length, 6)
        assert.equal(Object.keys(database2.index).length, 6)

        await dbInterface.write(database2, 22)

        await sleep(500)

        assert.equal(database2._oplog.length, 7)
        assert.equal(Object.keys(database2.index).length, 7)
        assert.equal(database2._oplog.heads.length, 1)

        const nextHashes = database2._oplog.heads[0].next
        const nextKeys = nextHashes.map(h => database2._oplog.get(h).payload.key)
        assert.equal(nextKeys.length, 2)
        assert.equal(nextKeys.includes('hello1D'), true)
        assert.equal(nextKeys.includes('hello2D'), true)
      })
    })

    describe('Forest log structure (fragmented log)', () => {
      beforeEach(async () => {
        // Add some docs to the local DB before remote node joins
        await dbInterface.write(database1, 1)
        await dbInterface.write(database1, 2)
        await dbInterface.write(database1, 3)
        await dbInterface.write(database1, 4)

        // Make sure all documents were inserted in DB1
        assert.equal(Object.values(database1.index).length, 4)
        assert.equal(database1._oplog.length, 4)

        // Open the DB on the second node, using 3 as the maximum replication
        // depth. Fetches the db manifest from the network
        const options = { sync: true, maxReplicationDepth: 3 }
        const address = database1.address.toString()
        database2 = await dbInterface.open(orbitdb2, address, options)

        // Give enough time for replication to happen
        await sleep(2000)

        assert.equal(database1._oplog.length, 4)
        assert.equal(Object.keys(database1.index).length, 4)
        assert.equal(database2._oplog.length, 3)
        assert.equal(Object.keys(database2.index).length, 3)
        assert.equal(database1.get('hello').length, 4)
        assert.equal(database1.get('hello1').length, 1)
        assert.equal(database2.get('hello').length, 3)
        assert.equal(database2.get('hello1').length, 0)

        // Disconnect peers
        await disconnectPeers(ipfs1, ipfs2)

        // Write to database1 while peers are disconnected
        // number of writes (4) > maxReplicationDepth (3)
        await dbInterface.write(database1, 5)
        await dbInterface.write(database1, 6)
        await dbInterface.write(database1, 7)
        await dbInterface.write(database1, 8)

        // Make sure documents were correctly inserted in DB1
        assert.equal(Object.values(database1.index).length, 8)
        assert.equal(database1._oplog.length, 8)

        // Reconnect peers
        await connectPeers(ipfs1, ipfs2)

        await sleep(500)

        // Now we make another write to trigger a publish to pubsub
        await dbInterface.write(database1, 9)
        // Make sure document was correctly inserted in DB1
        assert.equal(Object.values(database1.index).length, 9)
        assert.equal(database1._oplog.length, 9)

        // Give enough time for replication to happen on DB2
        await sleep(2000)

        // The log in database2 should look like this:
        //
        //    ----------------------------
        //   | 9 -> 8 -> 7    4 -> 3 -> 2 |
        //    ----------------------------
        //
      })

      it('should partially (according to limit) replicate the log and state', async () => {
        assert.equal(Object.values(database2.index).length, 6)
        assert.equal(database2._oplog.length, 6)

        assert.equal(database2.get('hello5').length, 0)
        assert.equal(database2.get('hello6').length, 0)
        assert.equal(database2.get('hello7').length, 1)
        assert.equal(database2.get('hello8').length, 1)
        assert.equal(database2.get('hello9').length, 1)

        assert.equal(logHas(database2._oplog, 'hello5'), false)
        assert.equal(logHas(database2._oplog, 'hello6'), false)
        assert.equal(logHas(database2._oplog, 'hello7'), true)
        assert.equal(logHas(database2._oplog, 'hello8'), true)
        assert.equal(logHas(database2._oplog, 'hello9'), true)
      })

      it('should successfully update log after partial replication', async () => {
        await dbInterface.write(database2, 22)

        // The log should now look like this:
        //
        //    -------------------------
        //   |   ---->  9 -> 8 -> 7    |
        //   |  |                      |
        //   | 22                      |
        //   |  |                      |
        //   |   ----->  4 -> 3 -> 2   |
        //    -------------------------
        //

        await sleep(500)

        assert.equal(database2._oplog.length, 7)
        assert.equal(Object.keys(database2.index).length, 7)
        assert.equal(database2._oplog.heads.length, 1)
        assert.equal(database2._oplog.heads[0].payload.key, 'hello22')

        const nextHashes = database2._oplog.heads[0].next
        const nextKeys = nextHashes.map(h => database2._oplog.get(h).payload.key)
        assert.equal(nextKeys.length, 2)
        assert.equal(nextKeys.includes('hello9'), true)
        assert.equal(nextKeys.includes('hello4'), true)
      })
    })
  })
})
