'use strict'

const assert = require('assert')
const mapSeries = require('p-each-series')
const rmrf = require('rimraf')
const OrbitDB = require('../src/OrbitDB')

// Include test utilities
const {
  config,
  startIpfs,
  stopIpfs,
  connectPeers,
  waitForPeers,
  testAPIs,
} = require('orbit-db-test-utils')

const dbPath1 = './orbitdb/tests/multiple-databases/1'
const dbPath2 = './orbitdb/tests/multiple-databases/2'
const ipfsPath1 = './orbitdb/tests/multiple-databases/1/ipfs'
const ipfsPath2 = './orbitdb/tests/multiple-databases/2/ipfs'

const dbInterface = {
    name: 'documents',
    open: async (orbitdb, address, options) => await orbitdb.docs(address, options),
    write: async (db, index) => await db.put({ _id: `hello${index}`, testing: index }),
    query: (db) => {
      const docs = db.get('hello')
      return docs ? docs[0].testing : 0
    }
  }

const compareIndices = (i1, i2) => {
  Object.keys(i1).forEach(k => {
    assert.deepEqual(i1[k], i2[k])
  })
  Object.keys(i2).forEach(k => {
    assert.deepEqual(i1[k], i2[k])
  })
}

Object.keys(testAPIs).forEach(API => {
  describe(`orbit-db - Index Snapshot (${API})`, function() {
    this.timeout(config.timeout)

    let ipfsd1, ipfsd2, ipfs1, ipfs2
    let orbitdb1, orbitdb2

    let localDatabase

    let numberOfEntries

    // Create two IPFS instances and two OrbitDB instances (2 nodes/peers)
    before(async () => {
      config.daemon1.repo = ipfsPath1
      config.daemon2.repo = ipfsPath2
      rmrf.sync(config.daemon1.repo)
      rmrf.sync(config.daemon2.repo)
      rmrf.sync(dbPath1)
      rmrf.sync(dbPath2)
      ipfsd1 = await startIpfs(API, config.daemon1)
      ipfsd2 = await startIpfs(API, config.daemon2)
      ipfs1 = ipfsd1.api
      ipfs2 = ipfsd2.api
      // Connect the peers manually to speed up test times
      await connectPeers(ipfs1, ipfs2)
      orbitdb1 = await OrbitDB.createInstance(ipfs1, { directory: dbPath1 })
      orbitdb2 = await OrbitDB.createInstance(ipfs2, { directory: dbPath2 })

      // Open the database on the first node
      const options = { create: true, accessController: { write: ['*'] } }
      localDatabase = await dbInterface.open(orbitdb1, dbInterface.name, options)

      // Add some docs to the local DB before remote node joins
      numberOfEntries = 7
      for (var i = 1; i <= numberOfEntries; i++) {
        await dbInterface.write(localDatabase, i)
      }

      // Make sure all documents were inserted in the DB
      assert.equal(Object.values(localDatabase.index).length, numberOfEntries)
    })

    after(async () => {
      await localDatabase.drop()

      if(orbitdb1)
        await orbitdb1.stop()

      if(orbitdb2)
        await orbitdb2.stop()

      if (ipfsd1)
        await stopIpfs(ipfsd1)

      if (ipfsd2)
        await stopIpfs(ipfsd2)
    })

    describe("Pass index snapshot IPFS hash at node's creation" , () => {
      let remoteDatabase

      after(async () => {
        await remoteDatabase.drop()
      })

      it('instantiates a remote docstore from the local Index snapshot', async () => {
        const snapshot = await localDatabase.saveIndexSnapshotToIPFS()

        // Open the DB on the second node, using the index snapshot
        // The second peer fetches the db manifest from the network and
        // uses the snapshot to initialize its index
        const options = { sync: true, indexSnapshot: snapshot.hash, replicate: false }
        const address = localDatabase.address.toString()
        remoteDatabase = await dbInterface.open(orbitdb2, address, options)

        // Without replication happening, see if remote index is the same as local index
        assert.equal(Object.values(localDatabase.index).length, Object.values(remoteDatabase.index).length)
        compareIndices(localDatabase.index, remoteDatabase.index)
        assert.deepEqual(localDatabase.index, remoteDatabase.index)
      })

      it('retrieves an object from the remote docstore', async () => {
        let retrievedObjects = remoteDatabase.get('hello')
        assert.equal(retrievedObjects.length, numberOfEntries)

        retrievedObjects = remoteDatabase.get(`hello${numberOfEntries}`)
        assert.equal(retrievedObjects.length, 1)
        assert.deepEqual(retrievedObjects[0], { _id: `hello${numberOfEntries}`, testing: numberOfEntries })
      })

      it('adds an object to the remote docstore', async () => {
        const newEntryID = 'newEntry'
        const newEntry = { _id: newEntryID, testing: -237 }
        await remoteDatabase.put(newEntry)

        assert.equal(Object.keys(remoteDatabase.index).length, Object.keys(localDatabase.index).length+1)
        assert.equal(remoteDatabase.get(newEntryID).length, 1)
        assert.deepEqual(remoteDatabase.get(newEntryID)[0], newEntry)
      })
    })

    describe('Get index snapshot IPFS hash during node joining protocol' , () => {
      let remoteDatabase

      after(async () => {
        await remoteDatabase.drop()
      })

      it('instantiates a remote docstore from the local Index snapshot', async () => {
        await localDatabase.saveIndexSnapshotToIPFS()

        // Open the DB on the second node, using the index snapshot (we say only that
        // that we want to initialize from a snapshot, and give no more info on it)
        // The second peer fetches the snapshot from the network and uses it to
        // initialize its index
        const options = { sync: true, indexSnapshot: true }
        const address = localDatabase.address.toString()
        remoteDatabase = await dbInterface.open(orbitdb2, address, options)

        // Wait for remote node to initialize index from snapshot
        await new Promise((resolve) => {
          const interval = setInterval(() => {
            if (Object.keys(remoteDatabase.index).length >= numberOfEntries) {
              clearInterval(interval)
              resolve()
            }
          }, 500)
        })

        // Without replication happening, see if remote index is the same as local index
        assert.equal(Object.values(localDatabase.index).length, Object.values(remoteDatabase.index).length)
        compareIndices(localDatabase.index, remoteDatabase.index)
        assert.deepEqual(localDatabase.index, remoteDatabase.index)
      })

      it('retrieves an object from the remote docstore', async () => {
        let retrievedObjects = remoteDatabase.get('hello')
        assert.equal(retrievedObjects.length, numberOfEntries)

        retrievedObjects = remoteDatabase.get(`hello${numberOfEntries}`)
        assert.equal(retrievedObjects.length, 1)
        assert.deepEqual(retrievedObjects[0], { _id: `hello${numberOfEntries}`, testing: numberOfEntries })
      })

      it('adds an object to the remote docstore', async () => {
        const newEntryID = 'newEntry'
        const newEntry = { _id: newEntryID, testing: -237 }
        await remoteDatabase.put(newEntry)

        assert.equal(Object.keys(remoteDatabase.index).length, Object.keys(localDatabase.index).length+1)
        assert.equal(remoteDatabase.get(newEntryID).length, 1)
        assert.deepEqual(remoteDatabase.get(newEntryID)[0], newEntry)
      })
    })

    describe('Check snapshot is stored correctly in IPFS', () => {
      let remoteDatabase
      let snapshot, indexSnapshot
      let indexSnapshot_localIPFS, indexSnapshot_remoteIPFS

      before(async () => {
        // Snapshot retrieved directly from local node
        indexSnapshot = await localDatabase.getIndexSnapshot()

        // Save local node's index snapshot to IPFS
        snapshot = await localDatabase.saveIndexSnapshotToIPFS()

        // Snapshot retrieved from IPFS on local node
        indexSnapshot_localIPFS = await localDatabase.getIndexSnapshotFromIPFS(snapshot.path)

        // Open DB on a second node
        const address = localDatabase.address.toString()
        const options = { sync: true, replicate: false }
        remoteDatabase = await dbInterface.open(orbitdb2, address, options)

        // Snapshot retrieved from IPFS on remote node
        indexSnapshot_remoteIPFS = await remoteDatabase.getIndexSnapshotFromIPFS(snapshot.path)
      })

      it('retrieved local IPFS snapshot is correct', async () => {
        assert.equal(indexSnapshot_localIPFS, indexSnapshot)
      })

      it('retrieved remote IPFS snapshot is correct', async () => {
        assert.equal(indexSnapshot_remoteIPFS, indexSnapshot)
      })

      it('retrieved local and remote IPFS snapshots are equal', async () => {
        assert.equal(indexSnapshot_remoteIPFS, indexSnapshot_localIPFS)
      })
    })
  })
})
