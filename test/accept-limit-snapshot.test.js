'use strict'

const assert = require('assert')
const rmrf = require('rimraf')
const OrbitDB = require('../src/OrbitDB')

// Include test utilities
const {
  config,
  startIpfs,
  stopIpfs,
  connectPeers,
  waitForPeers,
  testAPIs,
} = require('orbit-db-test-utils')

const dbPath1 = './orbitdb/tests/multiple-databases/1'
const dbPath2 = './orbitdb/tests/multiple-databases/2'
const ipfsPath1 = './orbitdb/tests/multiple-databases/1/ipfs'
const ipfsPath2 = './orbitdb/tests/multiple-databases/2/ipfs'

const dbInterface = {
    name: 'documents',
    open: async (orbitdb, address, options) => await orbitdb.docs(address, options),
    write: async (db, index) => await db.put({ _id: `hello${index}`, testing: index }),
    query: (db) => {
      const docs = db.get('hello')
      return docs ? docs[0].testing : 0
    }
  }

const sleep = ms => new Promise((resolve) => {
    setTimeout(resolve, ms);
  })

Object.keys(testAPIs).forEach(API => {
  describe(`orbit-db - Index Snapshot Accepting Limit (${API})`, function() {
    this.timeout(config.timeout)

    let ipfsd1, ipfsd2, ipfs1, ipfs2
    let orbitdb1, orbitdb2

    let localDatabase, remoteDatabase

    let numberOfEntries

    // Create two IPFS instances and two OrbitDB instances (2 nodes/peers)
    before(async () => {
      config.daemon1.repo = ipfsPath1
      config.daemon2.repo = ipfsPath2
      rmrf.sync(config.daemon1.repo)
      rmrf.sync(config.daemon2.repo)
      rmrf.sync(dbPath1)
      rmrf.sync(dbPath2)
      ipfsd1 = await startIpfs(API, config.daemon1)
      ipfsd2 = await startIpfs(API, config.daemon2)
      ipfs1 = ipfsd1.api
      ipfs2 = ipfsd2.api
      // Connect the peers manually to speed up test times
      await connectPeers(ipfs1, ipfs2)
      orbitdb1 = await OrbitDB.createInstance(ipfs1, { directory: dbPath1 })
      orbitdb2 = await OrbitDB.createInstance(ipfs2, { directory: dbPath2 })

      // Open the database on the first node
      const options = { create: true, accessController: { write: ['*'] } }
      localDatabase = await dbInterface.open(orbitdb1, dbInterface.name, options)

      // Add some docs to the local DB before remote node joins
      numberOfEntries = 7
      for (var i = 1; i <= numberOfEntries; i++) {
        await dbInterface.write(localDatabase, i)
      }

      // Make sure all documents were inserted in the DB
      assert.equal(Object.values(localDatabase.index).length, numberOfEntries)
    })

    after(async () => {
      await localDatabase.drop()
      if(remoteDatabase) await remoteDatabase.drop()

      if(orbitdb1)
        await orbitdb1.stop()

      if(orbitdb2)
        await orbitdb2.stop()

      if (ipfsd1)
        await stopIpfs(ipfsd1)

      if (ipfsd2)
        await stopIpfs(ipfsd2)
    })

    describe('Limit of 2 - only first two different snapshots are accepted' , () => {
      let snapshot1, snapshot2, snapshot3, address

      it('accepts first snapshot', async () => {
        snapshot1 = await localDatabase.saveIndexSnapshotToIPFS()

        // Open the DB on the second node, using the index snapshot (we say only that
        // that we want to initialize from a snapshot, and give no more info on it)
        // The second peer fetches the snapshot from the network and uses it to
        // initialize its index
        const options = { sync: true, indexSnapshot: true, maxAcceptedSnapshots: 2 }
        address = localDatabase.address.toString()
        remoteDatabase = await dbInterface.open(orbitdb2, address, options)

        assert.equal(orbitdb2.snapCounters[address], 0)
        assert.notEqual(orbitdb2.acceptedSnapshots[address][snapshot1.hash], true)

        // Wait for remote node to initialize index from snapshot
        await new Promise((resolve) => {
          const interval = setInterval(() => {
            if (Object.keys(remoteDatabase.index).length >= numberOfEntries) {
              clearInterval(interval)
              resolve()
            }
          }, 500)
        })

        // Without replication happening, see if remote index is the same as local index
        assert.equal(Object.values(localDatabase.index).length, Object.values(remoteDatabase.index).length)
        assert.deepEqual(localDatabase.index, remoteDatabase.index)

        assert.equal(orbitdb2.snapCounters[address], 1)
        assert.equal(orbitdb2.acceptedSnapshots[address][snapshot1.hash], true)
      })

      it('does not accept same snapshot again', async () => {
        // Trigger node 1 to send node 2 another snapshot
        // !! However, this is not expectable behavior !!
        await orbitdb1._onPeerConnected(address, orbitdb2.id)

        // Wait for remote node to receive snapshot
        await sleep(1000)

        assert.equal(orbitdb2.snapCounters[address], 1)
        assert.equal(orbitdb2.acceptedSnapshots[address][snapshot1.hash], true)
      })

      it('accepts new second snapshot', async () => {
        numberOfEntries++
        await dbInterface.write(localDatabase, numberOfEntries)

        snapshot2 = await localDatabase.saveIndexSnapshotToIPFS()

        assert.equal(orbitdb2.snapCounters[address], 1)
        assert.equal(orbitdb2.acceptedSnapshots[address][snapshot1.hash], true)
        assert.notEqual(orbitdb2.acceptedSnapshots[address][snapshot2.hash], true)

        // Trigger node 1 to send node 2 another snapshot
        // !! However, this is not expectable behavior !!
        await orbitdb1._onPeerConnected(address, orbitdb2.id)

        // Wait for remote node to receive new snapshot
        await new Promise((resolve) => {
          const interval = setInterval(() => {
            if (orbitdb2.acceptedSnapshots[address][snapshot2.hash] === true) {
              clearInterval(interval)
              resolve()
            }
          }, 250)
        })

        assert.equal(orbitdb2.snapCounters[address], 2)
        assert.equal(orbitdb2.acceptedSnapshots[address][snapshot1.hash], true)
        assert.equal(orbitdb2.acceptedSnapshots[address][snapshot2.hash], true)
      })

      it('does not accept new third snapshot', async () => {
        numberOfEntries++
        await dbInterface.write(localDatabase, numberOfEntries)

        snapshot3 = await localDatabase.saveIndexSnapshotToIPFS()

        assert.equal(orbitdb2.snapCounters[address], 2)
        assert.equal(orbitdb2.acceptedSnapshots[address][snapshot1.hash], true)
        assert.equal(orbitdb2.acceptedSnapshots[address][snapshot2.hash], true)
        assert.notEqual(orbitdb2.acceptedSnapshots[address][snapshot3.hash], true)

        // Trigger node 1 to send node 2 the third snapshot
        // !! However, this is not expectable behavior !!
        await orbitdb1._onPeerConnected(address, orbitdb2.id)

        // Wait for remote node to receive new snapshot
        await sleep(1000)

        assert.equal(orbitdb2.snapCounters[address], 2)
        assert.equal(orbitdb2.acceptedSnapshots[address][snapshot1.hash], true)
        assert.equal(orbitdb2.acceptedSnapshots[address][snapshot2.hash], true)
        assert.notEqual(orbitdb2.acceptedSnapshots[address][snapshot3.hash], true)
      })
    })
  })
})
