'use strict'

const assert = require('assert')
const mapSeries = require('p-each-series')
const rmrf = require('rimraf')
const OrbitDB = require('../src/OrbitDB')

const Logger = require('logplease')
const logger = Logger.create('test', { color: Logger.Colors.Red })
// Logger.setLogLevel('DEBUG')

// Include test utilities
const {
  config,
  startIpfs,
  stopIpfs,
  connectPeers,
  waitForPeers,
  testAPIs,
} = require('orbit-db-test-utils')

const daemon3 = {
    repo: './ipfs/ipfs-log/tests/daemon3',
    start: true,
    EXPERIMENTAL: {
      pubsub: true
    },
    config: {
      Addresses: {
        API: '/ip4/127.0.0.1/tcp/0',
        Swarm: ['/ip4/0.0.0.0/tcp/0'],
        Gateway: '/ip4/0.0.0.0/tcp/0'
      },
      Bootstrap: [],
      Discovery: {
        MDNS: {
          Enabled: true,
          Interval: 0
        },
        webRTCStar: {
          Enabled: false
        }
      }
    }
  }

const dbPath1 = './orbitdb/tests/multiple-databases/1'
const dbPath2 = './orbitdb/tests/multiple-databases/2'
const dbPath3 = './orbitdb/tests/multiple-databases/3'
const ipfsPath1 = './orbitdb/tests/multiple-databases/1/ipfs'
const ipfsPath2 = './orbitdb/tests/multiple-databases/2/ipfs'
const ipfsPath3 = './orbitdb/tests/multiple-databases/3/ipfs'

const dbInterface = {
    name: 'documents',
    open: async (orbitdb, address, options) => await orbitdb.docs(address, options),
    write: async (db, id, val) => await db.put({ _id: id, value: val }),
    query: (db) => {
      const docs = db.get('hello')
      return docs ? docs[0].testing : 0
    }
  }

const sleep = ms => new Promise((resolve) => {
    setTimeout(resolve, ms);
  })

const waitForReplication = async (log, assertions, { stopCount = 4, time = 150 } = {}) => {
  let prev = log.length
  let current = log.length
  let count = 0
  await new Promise((resolve) => {
    const interval = setInterval(() => {
      prev = current
      current = log.length
      if (prev < current) count = 0;
      else count++;
      if (count >= stopCount) {
        clearInterval(interval)
        if (assertions) assertions();
        resolve()
      }
    }, time)
  })
}

const waitForIndex = async (db, size, { time = 500 } = {}) => {
  await new Promise((resolve) => {
    const interval = setInterval(() => {
      if (Object.keys(db.index).length >= size) {
        clearInterval(interval)
        resolve()
      }
    }, time)
  })
}

const printLog = (log) => {
  log.values.reverse().forEach(entry => {
    console.log(`\n${entry.payload.key}=${entry.payload.value.value} (t=${entry.clock.time}):`)
    entry.next.forEach(hash => {
      const nextEntry = log.get(hash)
      if (nextEntry) console.log(`    --> ${nextEntry.payload.key}=${nextEntry.payload.value.value} (t=${nextEntry.clock.time})`)
    })
  })
}

Object.keys(testAPIs).forEach(API => {
  describe(`orbit-db - applyIndexToLog (this always happens now) (${API})`, function() {
    this.timeout(config.timeout)

    let ipfsd1, ipfsd2, ipfsd3, ipfs1, ipfs2, ipfs3
    let orbitdb1, orbitdb2, orbitdb3

    let database1, database2, database3

    let localHeads, remoteHeads

    const startIPFS1 = async () => {
      config.daemon1.repo = ipfsPath1
      rmrf.sync(config.daemon1.repo)
      rmrf.sync(dbPath1)
      ipfsd1 = await startIpfs(API, config.daemon1)
      ipfs1 = ipfsd1.api
    }

    const startIPFS2 = async () => {
      config.daemon2.repo = ipfsPath2
      rmrf.sync(config.daemon2.repo)
      rmrf.sync(dbPath2)
      ipfsd2 = await startIpfs(API, config.daemon2)
      ipfs2 = ipfsd2.api
    }

    const startIPFS3 = async () => {
      daemon3.repo = ipfsPath3
      rmrf.sync(daemon3.repo)
      rmrf.sync(dbPath3)
      ipfsd3 = await startIpfs(API, daemon3)
      ipfs3 = ipfsd3.api
    }

    // Create two IPFS instances and two OrbitDB instances (2 nodes/peers)
    beforeEach(async () => {
      await startIPFS1()
      await startIPFS2()
      await startIPFS3()

      // Connect the peers manually to speed up test times
      await connectPeers(ipfs1, ipfs2)
      await connectPeers(ipfs1, ipfs3)
      await connectPeers(ipfs2, ipfs3)

      orbitdb1 = await OrbitDB.createInstance(ipfs1, { directory: dbPath1 })
      orbitdb2 = await OrbitDB.createInstance(ipfs2, { directory: dbPath2 })
      orbitdb3 = await OrbitDB.createInstance(ipfs3, { directory: dbPath3 })
    })

    afterEach(async () => {
      if (database1)
        await database1.drop()
      if (database2)
        await database2.drop()
      if (database3)
        await database3.drop()

      if(orbitdb1)
        await orbitdb1.stop()
      if(orbitdb2)
        await orbitdb2.stop()
      if(orbitdb3)
        await orbitdb3.stop()

      if (ipfsd1)
        await stopIpfs(ipfsd1)
      if (ipfsd2)
        await stopIpfs(ipfsd2)
      if (ipfsd3)
        await stopIpfs(ipfsd3)
    })

    describe('2 nodes and old snapshot', () => {
      let snapshotSize

      beforeEach(async () => {
        // Open the database on the first node
        const options = { create: true, accessController: { write: ['*'] } }
        database1 = await dbInterface.open(orbitdb1, dbInterface.name, options)

        await dbInterface.write(database1, 'A', 0)
        await dbInterface.write(database1, 'B', 0)

        await database1.saveIndexSnapshotToIPFS()
        snapshotSize = 2

        await dbInterface.write(database1, 'A', 1)
        await dbInterface.write(database1, 'A', 2)
        await dbInterface.write(database1, 'B', 1)
        await dbInterface.write(database1, 'A', 3)
        await dbInterface.write(database1, 'A', 4)
        await dbInterface.write(database1, 'A', 5)
      })

      it('should reach incorrect state - too small maxReplicationDepth', async () => {
        // Open the DB on the second node from indexSnapshot
        // maxReplicationDepth of 3 is not enough to reach current state of both objects
        const address = database1.address.toString()
        const options = { sync: true, indexSnapshot: true, maxReplicationDepth: 3 }
        database2 = await dbInterface.open(orbitdb2, address, options)

        // Wait for node 2 to initialize index from snapshot
        await waitForIndex(database2, snapshotSize)

        await waitForReplication(database2._oplog)

        assert.equal(database1._oplog.length, 8)
        assert.equal(database2._oplog.length, 5)
        assert.notDeepEqual(database1.get(''), database2.get(''))
        assert.deepEqual(database2.get('B')[0].value, 0)
      })

      it('should reach correct state - appropriate maxReplicationDepth', async () => {
        // Open the DB on the second node from indexSnapshot
        // maxReplicationDepth of 4 is enough to reach current state of both objects
        const address = database1.address.toString()
        const options = { sync: true, indexSnapshot: true, maxReplicationDepth: 4 }
        database2 = await dbInterface.open(orbitdb2, address, options)

        // Wait for node 2 to initialize index from snapshot
        await waitForIndex(database2, snapshotSize)

        await waitForReplication(database2._oplog)

        assert.equal(database1._oplog.length, 8)
        assert.equal(database2._oplog.length, 6)
        assert.deepEqual(database1.get('A'), database2.get('A'))
        assert.deepEqual(database1.get('B'), database2.get('B'))
      })
    })

    // TODO: change these tests to use `indexSnapshot: true`
    describe('3 nodes - local and remote heads situation', () => {
      let snapshotSize

      beforeEach(async () => {
        // Expected behavior

        // -------NODE 1-------    -------NODE 2-------    -------NODE 3-------
        //       startup
        //                               startup
        //                  <--sync-->

        //    write A:=0 (t0)         write B:=0 (t0)

        //                  <--sync-->

        //  -heads-                 -heads-
        //  local: {A=0}            local: {B=0}
        //  remote: {A=0;B=0}       remote: {A=0;B=0}

        //                            write C:=1 (t1)
        //                            write B:=2 (t2)
        //                            write C:=2 (t3)
        //                            write C:=3 (t4)
        //                            write C:=4 (t5)

        //                  <--sync-->

        //  -heads-                 -heads-
        //  local: {A=0}            local: {C=4}
        //  remote: {C=4}           remote: {A=0;B=0}

        //                            takes snapshot

        //                                                   joins network

        //                         sends snapshot
        //                             --- {A:0, B:2, C:4} --->
        //                         and heads
        //                             --- {C=4; A=0; B=0} --->
        //    sends heads
        //               ------------ {A=0; C=4} ------------->
        //                                                merges snapshot in index and
        //                                                replicates log w/ depth 2 (from
        //                                                heads)

        //                                                !!! Heads/snapshot can arrive in
        //                                                any order, and depending on this,
        //                                                result will vary

        //                                                If N1's heads arrive first, the
        //                                                log will look like this
        //                                                [A=0] [B=0] [B=2]  C=3 <--[C=4]

        //                                                If N2's snapshot arrives first,
        //                                                the log will look like this
        //                                                [A=0] [B=0] [B=2] [C=4]

        //                                                the heads are in [square brackets]

        // Open the database on the first node
        let options = { create: true, accessController: { write: ['*'] } }
        database1 = await dbInterface.open(orbitdb1, dbInterface.name, options)

        // Open the DB on the second node
        const address = database1.address.toString()
        options = { sync: true }
        database2 = await dbInterface.open(orbitdb2, address, options)

        dbInterface.write(database1, 'A', 0)
        dbInterface.write(database2, 'B', 0)

        await waitForReplication(database1._oplog)
        await waitForReplication(database2._oplog)
        assert.equal(database1._oplog.length, 2)
        assert.equal(database2._oplog.length, 2)

        await dbInterface.write(database2, 'C', 1)
        await dbInterface.write(database2, 'B', 2)
        await dbInterface.write(database2, 'C', 2)
        await dbInterface.write(database2, 'C', 3)
        await dbInterface.write(database2, 'C', 4)

        await database2.saveIndexSnapshotToIPFS()
        snapshotSize = 3
      })

      it('third node should converge to correct state', async () => {
        await waitForReplication(database1._oplog)

        // Open the DB on the third node from indexSnapshot, with applyIndex
        // and maxReplicationDepth of 2 (would not be enough to get full state)
        const address = database1.address.toString()
        const options = { sync: true, indexSnapshot: true, maxReplicationDepth: 2 }
        database3 = await dbInterface.open(orbitdb3, address, options)

        await waitForIndex(database3, snapshotSize)
        await waitForReplication(database3._oplog)

        // Wait for Node 1 and 2 to send heads/snapshot
        if (database3._oplog.length < 4)
          await sleep(1000)

        await waitForReplication(database3._oplog)

        assert(database3._oplog.length == 4 || database3._oplog.length == 5)
        assert.equal(Object.values(database3.index).length, 3)

        assert.equal(database3._oplog.heads.length, 4)

        const headsIncludes = (entry) => {
          return database3._oplog.heads.reduce((res, e) => {
            return res || (e.payload.value._id === entry._id &&
                            e.payload.value.value === entry.value)
          }, false)
        }

        // Heads should be: [A=0] [B=0] [B=2] [C=4]
        assert(headsIncludes({ _id: 'A', value: 0 }))
        assert(headsIncludes({ _id: 'B', value: 0 }))
        assert(headsIncludes({ _id: 'B', value: 2 }))
        assert(headsIncludes({ _id: 'C', value: 4 }))

        assert.equal(database3.get('A')[0].value, 0)
        assert.equal(database3.get('B')[0].value, 2)
        assert.equal(database3.get('C')[0].value, 4)
      })
    })
  })
})
