'use strict'

const assert = require('assert')
const mapSeries = require('p-each-series')
const rmrf = require('rimraf')
const OrbitDB = require('../src/OrbitDB')

// Include test utilities
const {
  config,
  startIpfs,
  stopIpfs,
  connectPeers,
  waitForPeers,
  testAPIs,
} = require('orbit-db-test-utils')

const dbPath1 = './orbitdb/tests/multiple-databases/1'
const ipfsPath1 = './orbitdb/tests/multiple-databases/1/ipfs'

const dbInterface = {
    name: 'documents',
    open: async (orbitdb, address, options) => await orbitdb.docs(address, options),
    write: async (db, index) => await db.put({ _id: `hello${index}`, testing: index }),
    query: (db) => {
      const docs = db.get('hello')
      return docs ? docs[0].testing : 0
    }
  }

const sleep = ms => new Promise((resolve) => { setTimeout(resolve, ms) })

const errorMsg = 'There is no snapshot here...'
const getCurrentSnapshot = async (db) => {
  const snapshot = await db._cache.get(db.indexSnapshotPath)
  if (!snapshot) throw new Error(errorMsg)

  const indexSnapshot = await db.getIndexSnapshotFromIPFS(snapshot)
  return db._indexFromSnapshot(indexSnapshot)
}

Object.keys(testAPIs).forEach(API => {
  describe(`orbit-db - Snapshot-taking Heuristics (${API})`, function() {
    this.timeout(config.timeout)

    let ipfsd1, ipfs1, orbitdb1 ,database
    let numberOfEntries

    beforeEach(async () => {
      config.daemon1.repo = ipfsPath1
      rmrf.sync(config.daemon1.repo)
      rmrf.sync(dbPath1)
      ipfsd1 = await startIpfs(API, config.daemon1)
      ipfs1 = ipfsd1.api
      orbitdb1 = await OrbitDB.createInstance(ipfs1, { directory: dbPath1 })
    })

    afterEach(async () => {
      if (database)
        await database.drop()

      if(orbitdb1)
        await orbitdb1.stop()

      if (ipfsd1)
        await stopIpfs(ipfsd1)
    })

    describe('Every 5 log entries' , () => {
      beforeEach(async () => {
        const snapshotHeuristics = {
          type: 'LogSize',
          limit: 5
        }

        // Open the database on the first node
        const options = { create: true, accessController: { write: ['*'] }, snapshotHeuristics }
        database = await dbInterface.open(orbitdb1, dbInterface.name, options)
      })

      it('should have no snapshot after 4 inserts', async () => {
        // Add 4 docs to the DB
        numberOfEntries = 4
        for (var i = 1; i <= numberOfEntries; i++) {
          await dbInterface.write(database, i)
        }

        // Make sure all documents were inserted in the DB
        assert.equal(Object.values(database.index).length, numberOfEntries)

        // Give enough time for a snapshot to have been created and stored
        await sleep(500)

        await assert.rejects(
          async () => await getCurrentSnapshot(database),
          {
            name: 'Error',
            message: errorMsg
          })
      })

      it('should have a snapshot after 5 inserts', async () => {
        // Add 5 docs to the DB
        numberOfEntries = 5
        for (var i = 1; i <= numberOfEntries; i++) {
          await dbInterface.write(database, i)
        }

        // Make sure all documents were inserted in the DB
        assert.equal(Object.values(database.index).length, numberOfEntries)

        // Give enough time for a snapshot to be created and stored
        await sleep(500)

        const snapshot = await getCurrentSnapshot(database)

        assert(snapshot)
        assert.equal(Object.keys(snapshot).length, numberOfEntries)
      })

      it('should trigger <snapshot.taken> event', () => {

        return new Promise(async resolve => {
          database.events.on('snapshot.taken', resolve)

          // Add 5 docs to the DB
          numberOfEntries = 5
          for (var i = 1; i <= numberOfEntries; i++) {
            await dbInterface.write(database, i)
          }
        })
      })
    })

    describe('Every 1000 milliseconds' , () => {
      beforeEach(async () => {
        const snapshotHeuristics = {
          type: 'TimeInterval',
          period: 1000
        }

        // Open the database on the first node
        const options = { create: true, accessController: { write: ['*'] }, snapshotHeuristics }
        database = await dbInterface.open(orbitdb1, dbInterface.name, options)
      })

      it('should take snapshot before 1000ms timeout', (done) => {
        let timeout

        const success = async () => {
          const snapshot = await getCurrentSnapshot(database)
          assert.equal(Object.keys(snapshot).length, numberOfEntries)
          clearTimeout(timeout)
          done()
        }

        const fail = () => {
          done(new Error('Did not take snapshot in time'))
        }

        database.events.on('snapshot.taken', success)

        // Give a small margin to consider snapshot creation time
        timeout = setTimeout(fail, 1100)

        // Add 3 docs to the DB
        numberOfEntries = 3
        for (var i = 1; i <= numberOfEntries; i++) {
          dbInterface.write(database, i)
        }
      })

      it('should have taken 2 snapshots after 2000ms', (done) => {
        let snapshot, timeout

        const success = async () => {
          if (snapshot) {
            const newSnapshot = await getCurrentSnapshot(database)
            assert.notDeepEqual(snapshot, newSnapshot)
            assert.equal(Object.keys(newSnapshot).length, numberOfEntries*2)
            clearTimeout(timeout)
            done()
          } else {
            snapshot = await getCurrentSnapshot(database)
            assert.equal(Object.keys(snapshot).length, numberOfEntries)
            // Add 3 more docs to the DB
            numberOfEntries = 3
            for (var i = numberOfEntries; i <= numberOfEntries*2; i++) {
              dbInterface.write(database, i)
            }
          }
        }

        const fail = () => {
          done(new Error('Did not take snapshot in time'))
        }

        database.events.on('snapshot.taken', success)

        // Give a small margin to consider snapshot creation time
        timeout = setTimeout(fail, 2200)

        // Add 3 docs to the DB
        numberOfEntries = 3
        for (var i = 1; i <= numberOfEntries; i++) {
          dbInterface.write(database, i)
        }
      })
    })
  })
})
