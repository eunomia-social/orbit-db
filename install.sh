# install.sh

# git clone https://git-ext.inov.pt/cyber/eunomia/orbit-db.git
# git clone https://git-ext.inov.pt/cyber/eunomia/orbit-db-store.git
# git clone https://git-ext.inov.pt/cyber/eunomia/orbit-db-docstore.git

cd ../orbit-db-store
npm ci
cd ../orbit-db-docstore
npm ci
cd ../orbit-db
npm ci
npm link ../orbit-db-store
npm link ../orbit-db-docstore
cd ../orbit-db-docstore
npm link ../orbit-db-store
cd ../orbit-db
