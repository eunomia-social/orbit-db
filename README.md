Original orbit-db README [here](./orig-README.md).

# Index snapshot support for OrbitDB

## Installation
- Install [node.js and npm](https://docs.npmjs.com/downloading-and-installing-node-js-and-npm) (in case you haven't already)
- Clone this repository (`orbit-db`), `orbit-db-store` ([link](https://git-ext.inov.pt/cyber/eunomia/orbit-db-store)) and `orbit-db-docstore` ([here](https://git-ext.inov.pt/cyber/eunomia/orbit-db-docstore)), and place them in the same directory.
  ```bash
  git clone https://git-ext.inov.pt/cyber/eunomia/orbit-db.git
  git clone https://git-ext.inov.pt/cyber/eunomia/orbit-db-store.git
  git clone https://git-ext.inov.pt/cyber/eunomia/orbit-db-docstore.git
  ```
- Run the install script inside the `orbit-db` directory
  ```bash
  cd orbit-db
  bash install.sh
  ```
- (Make sure every repository is checked out to the latest branch)
- Run `npm test` to make sure everything is working


For the `npm link` command to work properly, the directory structure should look like this:
```bash
├── orbit-db
├── orbit-db-docstore
└── orbit-db-store
```

## Documentation
The following documents relate to the snapshot feature.

- A brief explanation of how OrbitDB works (focusing on replication) - [orbit-manual](./docs/orbit-manual.md)
- Explanation of snapshot-related tests - [test-GUIDE](./docs/tests-GUIDE.md)
- Some considerations and scenarios where the snapshot feature affects consistency - [consistency](./docs/consistency.md)
- Additional disorganized information - [orbit-snapshots](./docs/orbit-snapshots.md)
