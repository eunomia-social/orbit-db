'use strict'

const SnapshotHeuristics = require('./SnapshotHeuristics')

/**
 * Takes a snapshot every time the size of the operation log grows
 * a certain amount since the last snapshot. It checks the size of
 * the log every time the index is updated and compares it with its
 * size at the time the last snapshot was taken.
 */
class LogSize extends SnapshotHeuristics {
  constructor (store, options) {
    super(store, options)
    this.limit = options.limit || 5
    this.prevSize = 0

    // These are events associated with changing the state of the
    // database - _updateIndex is called
    this.store.events.on('write', this._evaluate.bind(this))
    this.store.events.on('replicated', this._evaluate.bind(this))
    this.store.events.on('ready', this._evaluate.bind(this))
  }

  /**
   * Compares the current size of the log with its size at the time
   * of the last snapshot (`prevSize`). If the size difference is
   * greater than or equal to the defined limit (`limit`), a snapshot
   * is taken and `prevSize` is updated.
   */
  async _evaluate () {
    const curSize = this.store._oplog.length
    if ((curSize - this.prevSize) >= this.limit) {
      this.prevSize = curSize
      await this._takeSnapshot()
    }
  }

  stop () {
    super.stop()
    this.store.events.removeListener('write', this._evaluate.bind(this))
    this.store.events.removeListener('replicated', this._evaluate.bind(this))
    this.store.events.removeListener('ready', this._evaluate.bind(this))
  }
}

module.exports = LogSize
