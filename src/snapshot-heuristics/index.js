'use strict'

const heuristics = {
  LogSize: require('./LogSize'),
  TimeInterval: require('./TimeInterval')
  // add more here
}

const snapshotHeuristics = (store, options) => {
  if (!options || !options.type)
    throw new Error('snapshot-heuristics: Invalid options object')

  if (!heuristics[options.type])
    throw new Error('snapshot-heuristics: Invalid heuristic type')

  return new heuristics[options.type](store, options)
}

module.exports = snapshotHeuristics
