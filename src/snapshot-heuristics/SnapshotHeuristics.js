'use strict'

const Logger = require('logplease')
const logger = Logger.create('orbit-db.snapshot-heuristics', { color: Logger.Colors.Cyan })
Logger.setLogLevel('ERROR')

/**
 * This class stands as an abstract class for implementing snapshot-taking
 * heuristics - decide when a snapshot should be taken automatically.
 */
class SnapshotHeuristics {
  constructor (store, options) {
    this.store = store
    this.options = options
  }

  /**
   * Invokes the Store's method to take an index snapshot and emits an
   * event to signal that the snapshot was taken
   */
  async _takeSnapshot () {
    logger.debug(`Taking a snapshot (triggered by heuristic)`)
    const snapshotInfo = await this.store.saveIndexSnapshotToIPFS()
    this.store.events.emit('snapshot.taken', snapshotInfo)
  }

  stop () {
    this.store.events.removeAllListeners('snapshot.taken')
  }
}

module.exports = SnapshotHeuristics
