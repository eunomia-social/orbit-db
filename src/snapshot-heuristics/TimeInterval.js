'use strict'

const SnapshotHeuristics = require('./SnapshotHeuristics')

/**
 * Takes a snapshot every `period` milliseconds.
 */
class TimeInterval extends SnapshotHeuristics {
  constructor (store, options) {
    super(store, options)
    this.period = options.period || 5000 // in milliseconds
    
    this.interval = setInterval(this._takeSnapshot.bind(this), this.period)
  }

  stop () {
    super.stop()
    clearInterval(this.interval)
  }
}

module.exports = TimeInterval
